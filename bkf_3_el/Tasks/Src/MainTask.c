/**
 ******************************************************************************
 * File Name          : freertos.c
 * Description        : Code for freertos applications
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

#include "BK_3el.h"

extern osThreadId IndikTaskHandle;
extern osThreadId EMULTaskHandle;
extern uint32_t MASSIV_1_F1[64] , MASSIV_2_F1[64] , MASSIV_1_F2[64] , MASSIV_2_F2[64] , MASSIV_1_F3[64] ,
		MASSIV_2_F3[64];
extern uint32_t MASSIV_1_OS[64] , MASSIV_2_OS[64] , MASSIV_1_TOK[64] , MASSIV_2_TOK[64] , REZ[64];

xSemaphoreHandle xStartCalculate;
xQueueHandle Ququ_flash;
xQueueHandle Ququ_indik;
xQueueHandle Ququ_Uart;
osThreadId defaultTaskHandle;
osThreadId EMULTask1Handle;
RTC_HandleTypeDef hrtc;

uint32_t * mP_F1;
uint32_t * mP_F2;
uint32_t * mP_F3;
uint32_t * mP_OS;
uint32_t * mP_TOK;

BK_3L BK_3={"BK-3el;0.01",& BK_3.F1,& BK_3.F2,& BK_3.F3,& BK_3.F_OS,& BK_3.F_TOK};

unsigned int volatile A=0;
float REAL , IMAGE , AMPL_F1 , FI_F1 , AMPL_F2 , FI_F2 , AMPL_F3 , FI_F3 , AMPL_OS , FI_OS , AMPL_TOK ,
		FI_TOK;

uint32_t * P_F1;
uint32_t * P_F2;
uint32_t * P_F3;
uint32_t * P_OS;
uint32_t * P_TOK;


uint8_t volatile FLAG1=0;

static void UPDATE_Bootloader(void);
static void CalculatIndikValues(BK_3L *d);

unsigned int volatile CO=0;
/**
 * @brief  ��������� ��������� �������� ���������� ��� ��������� ����� ����� ������ �������
 */
static void CalculatIndikValues(BK_3L *d){
	if((d->F1.StatusFaz.Indikat_value > (d->F1.StatusFaz.Effecient_value + 10.))
			|| (d->F1.StatusFaz.Indikat_value < (d->F1.StatusFaz.Effecient_value - 10.))){
		d->F1.StatusFaz.Indikat_value=d->F1.StatusFaz.Effecient_value;
	}else{
		d->F1.StatusFaz.Indikat_value=
				((d->F1.StatusFaz.Effecient_value - d->F1.StatusFaz.Indikat_value) / 70.)
						+ d->F1.StatusFaz.Indikat_value;
	}

	if((d->F2.StatusFaz.Indikat_value > (d->F2.StatusFaz.Effecient_value + 10.))
			|| (d->F2.StatusFaz.Indikat_value < (d->F2.StatusFaz.Effecient_value - 10.))){
		d->F2.StatusFaz.Indikat_value=d->F2.StatusFaz.Effecient_value;
	}else{
		d->F2.StatusFaz.Indikat_value=
				((d->F2.StatusFaz.Effecient_value - d->F2.StatusFaz.Indikat_value) / 70.)
						+ d->F2.StatusFaz.Indikat_value;
	}

	if((d->F3.StatusFaz.Indikat_value > (d->F3.StatusFaz.Effecient_value + 10.))
			|| (d->F3.StatusFaz.Indikat_value < (d->F3.StatusFaz.Effecient_value - 10.))){
		d->F3.StatusFaz.Indikat_value=d->F3.StatusFaz.Effecient_value;
	}else{
		d->F3.StatusFaz.Indikat_value=
				((d->F3.StatusFaz.Effecient_value - d->F3.StatusFaz.Indikat_value) / 70.)
						+ d->F3.StatusFaz.Indikat_value;
	}

	if((d->F_TOK.StatusFaz.Indikat_value > (d->F_TOK.StatusFaz.Effecient_value + 0.3))
			|| (d->F_TOK.StatusFaz.Indikat_value < (d->F3.StatusFaz.Effecient_value - 0.3))){
		d->F_TOK.StatusFaz.Indikat_value=d->F_TOK.StatusFaz.Effecient_value;
	}else{
		d->F_TOK.StatusFaz.Indikat_value=((d->F_TOK.StatusFaz.Effecient_value
				- d->F_TOK.StatusFaz.Indikat_value) / 70.) + d->F_TOK.StatusFaz.Indikat_value;
	}
}
//============================================================================================================
void vTaskMain(void *pvParametrs){
	unsigned int volatile B=0 , J;
	BK_3L *d=(BK_3L *)pvParametrs;

	device_init();

	osThreadDef(INDIKTask,vTask_INDIKACIA,osPriorityAboveNormal,0,128 + 64);
	IndikTaskHandle=osThreadCreate(osThread(INDIKTask), & BK_3);
	d->Stek_t.Heap=xPortGetFreeHeapSize();

#ifdef USART_DEBUG
	osThreadDef(EMULTask,vTask_EMULATION,osPriorityBelowNormal,0,128 );
	EMULTaskHandle=osThreadCreate(osThread(EMULTask), & BK_3);
	d->Stek_t.Heap=xPortGetFreeHeapSize();
	osThreadDef(EMULTask1,vTask_EMULATION_1,osPriorityBelowNormal,0,128 );
	EMULTask1Handle=osThreadCreate(osThread(EMULTask1), & BK_3);
#endif
	d->Stek_t.Heap=xPortGetFreeHeapSize();

	while(1){
		xSemaphoreTake(xStartCalculate,portMAX_DELAY);
//-----------------------------------------------------------------

		if(FLAG1 == 0){
			mP_F1=MASSIV_2_F1;
			mP_F2=MASSIV_2_F2;
			mP_F3=MASSIV_2_F3;
			mP_OS=MASSIV_2_OS;
			mP_TOK=MASSIV_2_TOK;
		}else{
			mP_F1=MASSIV_1_F1;
			mP_F2=MASSIV_1_F2;
			mP_F3=MASSIV_1_F3;
			mP_OS=MASSIV_1_OS;
			mP_TOK=MASSIV_1_TOK;
		}
//-----------------------------------------------------------------
		cr4_fft_64_stm32(REZ,mP_F1,64);
		REAL=(signed short)(REZ[1] & 0xFFFF);
		IMAGE=(signed short)(REZ[1] >> 16);
		AMPL_F1=sqrt((REAL * REAL) + (IMAGE * IMAGE));
		FI_F1=atan2f(IMAGE,REAL) * 180. / M_PI;
//-----------------------------------------------------------------
		cr4_fft_64_stm32(REZ,mP_F2,64);
		REAL=(signed short)(REZ[1] & 0xFFFF);
		IMAGE=(signed short)(REZ[1] >> 16);
		AMPL_F2=sqrt((REAL * REAL) + (IMAGE * IMAGE));
		FI_F2=atan2f(IMAGE,REAL) * 180. / M_PI;
//-----------------------------------------------------------------
		cr4_fft_64_stm32(REZ,mP_F3,64);
		REAL=(signed short)(REZ[1] & 0xFFFF);
		IMAGE=(signed short)(REZ[1] >> 16);
		AMPL_F3=sqrt((REAL * REAL) + (IMAGE * IMAGE));
		FI_F3=atan2f(IMAGE,REAL) * 180. / M_PI;
//-----------------------------------------------------------------
		cr4_fft_64_stm32(REZ,mP_OS,64);
		REAL=(signed short)(REZ[1] & 0xFFFF);
		IMAGE=(signed short)(REZ[1] >> 16);
		AMPL_OS=sqrt((REAL * REAL) + (IMAGE * IMAGE));
		FI_OS=atan2f(IMAGE,REAL) * 180. / M_PI;
//-----------------------------------------------------------------
		cr4_fft_64_stm32(REZ,mP_TOK,64);
		REAL=(signed short)(REZ[1] & 0xFFFF);
		IMAGE=(signed short)(REZ[1] >> 16);
		AMPL_TOK=sqrt((REAL * REAL) + (IMAGE * IMAGE));
		FI_TOK=atan2f(IMAGE,REAL) * 180. / M_PI;
		B=(uint16_t) ADC1->DR;
//-----------------------------------------------------------------
		FI_F2-=FI_F1;
		FI_F3-=FI_F1;
		FI_OS-=FI_F1;
		FI_TOK-=FI_F1;
		FI_F1=0;
//-----------------------------------------------------------------
		d->F1.StatusFaz.Effecient_value=AMPL_F1 * d->F1.SettingFaz.Koeff_mashtabirovania;
		d->F1.StatusFaz.Fase=FI_F1;
		d->F2.StatusFaz.Effecient_value=AMPL_F2 * d->F2.SettingFaz.Koeff_mashtabirovania;
		d->F2.StatusFaz.Fase=FI_F2;
		d->F3.StatusFaz.Effecient_value=AMPL_F3 * d->F3.SettingFaz.Koeff_mashtabirovania;
		d->F3.StatusFaz.Fase=FI_F3;
		d->F_OS.StatusFaz.Effecient_value=AMPL_OS * d->F_OS.SettingFaz.Koeff_mashtabirovania;
		d->F_OS.StatusFaz.Fase=FI_OS;
		d->F_TOK.StatusFaz.Effecient_value=AMPL_TOK * d->F_TOK.SettingFaz.Koeff_mashtabirovania;
		d->F_TOK.StatusFaz.Fase=FI_TOK;
//-----------------��������----------------------------------------
		d->F1.StatusFaz.Effecient_value=d->test.F1_EffectValue;
		d->F2.StatusFaz.Effecient_value=d->test.F2_EffectValue;
		d->F3.StatusFaz.Effecient_value=d->test.F3_EffectValue;
		d->F_OS.StatusFaz.Effecient_value=d->test.OS_EffectValue;
		if(d->F_OS.StatusFaz.Effecient_value > d->F_OS.SettingFaz.Level_of_presence){
			d->F_TOK.StatusFaz.Effecient_value=d->test.Tok_EffectValue;
		}else d->F_TOK.StatusFaz.Effecient_value=0;
//TODO
		CO++;
//-----------------------------------------------------------------
		CalculatIndikValues(d);
//-----------------------------------------------------------------
		Stalking( & d->F1);
		Stalking( & d->F2);
		Stalking( & d->F3);
		Stalking( & d->F_OS);
		Stalking( & d->F_TOK);
		Operacia();
		IndikaciaTokVoltage();

//-----------------------------------------------------------------
		BK_3.Stek_t.MainTaskStek=uxTaskGetStackHighWaterMark( NULL);
//-----------------------------------------------------------------
	}
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
