/*
 * handler.h
 *
 *  Created on: 25 окт. 2013 г.
 *      Author: develop2
 */

#ifndef HANDLER_H_
#define HANDLER_H_
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include <INIT.h>
#include "port.h"
#include "main.h"

#define PACKET_SIZE		150
typedef struct /*x_packet*/ {
	uint8_t id;
	uint8_t command;
	uint8_t length;
	uint8_t buffer[PACKET_SIZE];
} X_PACKET_T;


void device_handler ( );
void usb_handler ( uint8_t* buffer , uint16_t count );
void SendMessageToIndikacia ( CMD_INDIKACII_T A );
void SendMessageToFlash ( CMD_FLASH_T A );
void Otkluchit6_all ( void );
uint8_t get_crc8 ( uint8_t *psource , uint16_t size );
void USB_Handler ( uint8_t* buffer , uint16_t count );
void FillSettingsFromUsb(uint8_t* buffer);
void FillBufferFromSettings(uint8_t* buffer);
void ParseInformatProtocol(uint8_t* buf,uint32_t len);
static uint32_t STM32_SOFT_CRC32_1(uint8_t* sours,uint32_t Len);
void StartDownload(uint8_t * buffer);
void CopyImageBlock(uint8_t * buffer);
void UploadSoft(Commands command);
uint16_t GetPoolData(uint8_t * buffer);

#endif /* HANDLER_H_ */
