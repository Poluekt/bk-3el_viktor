/*
 * ADC.c
 *
 *  Created on: 28 янв. 2016 г.
 *      Author: Viktor
 */

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "INIT.h"
#include "main_proc.h"

extern uint32_t MASSIV_1_F1[64] , MASSIV_2_F1[64] , MASSIV_1_F2[64] , MASSIV_2_F2[64] , MASSIV_1_F3[64];
extern uint32_t MASSIV_1_OS[64] , MASSIV_2_OS[64] , MASSIV_1_TOK[64] , MASSIV_2_TOK[256] , MASSIV_2_F3[64];
extern uint32_t * P_F1;
extern uint32_t * P_F2;
extern uint32_t * P_F3;
extern uint32_t * P_OS;
extern uint32_t * P_TOK;
extern uint8_t volatile FLAG1;
extern xSemaphoreHandle xStartCalculate;

static uint16_t Counter1;

void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef* hadc){
// ПРОИЗОШЕЛ ВЫХОД ЗП ПРЕДЕЛЫ ЛИМИТОВ ЗНАЧЕНИЯ ТОКА -СВЕРХТОК
	ProcSetAvost();
}
void HAL_ADCEx_InjectedConvCpltCallback(ADC_HandleTypeDef* hadc){
	BaseType_t xHigherPriorityTaskWoken;
	xHigherPriorityTaskWoken= pdTRUE;

	* P_F1++= ADC2->JDR1;
	* P_F2++= ADC2->JDR2;
	* P_F3++= ADC2->JDR3;
	* P_OS++= ADC2->JDR4;
	* P_TOK++=(uint16_t) ADC1->DR;

	Counter1++;

	if(Counter1 == 64){
		Counter1=0;
		if(FLAG1 == 0){
			FLAG1=1;
			P_F1=MASSIV_2_F1;
			P_F2=MASSIV_2_F2;
			P_F3=MASSIV_2_F3;
			P_OS=MASSIV_2_OS;
			P_TOK=MASSIV_2_TOK;
		}else{
			FLAG1=0;
			P_F1=MASSIV_1_F1;
			P_F2=MASSIV_1_F2;
			P_F3=MASSIV_1_F3;
			P_OS=MASSIV_1_OS;
			P_TOK=MASSIV_1_TOK;
		}
		xSemaphoreGiveFromISR(xStartCalculate, & xHigherPriorityTaskWoken);
	}
}
