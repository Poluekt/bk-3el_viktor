/*
 * EmulationTask.c
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */
#include "BK_3el.h"

#ifdef USART_DEBUG

#define NORM_W  1
#define SVAR    0
#define OBRIV   0
#define TOK     0
#define TEST_USB1     1
extern xQueueHandle Ququ_Uart;
extern BK_3L BK_3;
extern unsigned int volatile CO;
extern uint8_t UserRxBufferFS[128];
static void OtkluchenieWaitGoodVkluchenia(BK_3L *d);
static void OtkluchenieWithSvarivanie(BK_3L *d);
static void OtkluchenieWaitObrivRele(BK_3L *d);

extern void LcdInit(void);
extern void WriteStrToLcd(uint8_t LINE_NUMBER,uint8_t POS,char * str);
extern void LcdDelay(uint32_t volatile DELAY);
extern X_PACKET_T packet;
extern  uint8_t GetCrc8(char *psource,uint8_t size);
//---------------------------------------------------------------------------------
#if TEST_USB1
//---------------------------------------------------------------------------------
void vTask_EMULATION_1(void const * argument){
	BK_3L *d=(BK_3L *)argument;
// задача производит тестирование алгоритма передачи по усб
	X_PACKET_T T;
#if 0
	Setting_Faz_T F1,F2,F3,F4,F5;
    SETTING_T B;

    while(1){
    T.id=0xAB;
    T.command=GET_SETTING;
    T.length=0;
    uint8_t crc8=GetCrc8((char*) & T,T.length + 3);
    T.buffer[0]=crc8;
    memcpy(  & UserRxBufferFS,&T,4);
    ParseInformatProtocol((uint8_t*) &T,4);

	 uint8_t J=0;
	memcpy( & B, & packet.buffer[(  J)],sizeof(SETTING_T));
	 J=(  J) + sizeof(SETTING_T);
	memcpy( & F1, & packet.buffer[(  J)],sizeof(Setting_Faz_T));
	 J=(  J) + sizeof(Setting_Faz_T);
	memcpy( & F2, & packet.buffer[( J)],sizeof(Setting_Faz_T));
	 J=(  J) + sizeof(Setting_Faz_T);
	memcpy( & F3, & packet.buffer[(  J)],sizeof(Setting_Faz_T));
	 J=(  J) + sizeof(Setting_Faz_T);
	memcpy( & F4, & packet.buffer[(  J)],sizeof(Setting_Faz_T));
	 J=(  J) + sizeof(Setting_Faz_T);
	memcpy( & F5, & packet.buffer[(  J)],sizeof(Setting_Faz_T));
	 J=(  J) + sizeof(Setting_Faz_T);

//------------включение первоначальное------
	osDelay(3000);

//------------ДЕЛАЕМ ЗАПРОС МАССИВОВ СЕТТИНГОВ--------------------

//------------------конец-----------------
		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
#endif

#if 0
    Status_Faz_T F1,F2,F3,F4,F5;
    BK_3_T B;
    while(1){
    T.id=0xAB;
    T.command=GET_STATE;
    T.length=0;
    uint8_t crc8=GetCrc8((char*) & T,T.length + 3);
    T.buffer[0]=crc8;
    memcpy(   UserRxBufferFS,&T,4);
    ParseInformatProtocol((uint8_t*) UserRxBufferFS,4);

    uint8_t * P=packet.buffer;
    uint8_t J=0;
	memcpy( & B,P,sizeof(BK_3_T));
	 J+=sizeof(BK_3_T);
	P =(uint8_t*)((uint32_t)P +sizeof(BK_3_T));
	memcpy( & F1,P,sizeof(Status_Faz_T));
	 J+=sizeof(Status_Faz_T);
	 P =(uint8_t*)((uint32_t)P +sizeof(Status_Faz_T));
	memcpy( & F2,P,sizeof(Status_Faz_T));
	 J+=sizeof(Status_Faz_T);
	 P =(uint8_t*)((uint32_t)P +sizeof(Status_Faz_T));
	memcpy( & F3,P,sizeof(Status_Faz_T));
	 J+=sizeof(Status_Faz_T);
	 P =(uint8_t*)((uint32_t)P +sizeof(Status_Faz_T));
	memcpy( & F4,P,sizeof(Status_Faz_T));
	 J+=sizeof(Status_Faz_T);
	P =(uint8_t*)((uint32_t)P +sizeof(Status_Faz_T));
	memcpy( & F5,P,sizeof(Status_Faz_T));
	 J+=sizeof(Status_Faz_T);
	 P =(uint8_t*)((uint32_t)P +sizeof(Status_Faz_T));

//------------включение первоначальное------
	osDelay(3000);

//------------ДЕЛАЕМ ЗАПРОС МАССИВОВ СЕТТИНГОВ--------------------

//------------------конец-----------------
		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
#endif

#if 0
    BK_3.BK3status.WriteAddress=I2C_IMAGE_START_ADDRESS-16;
while(1){

	SendMessageToFlash(store_moment_3);

	if((BK_3.BK3status.WriteAddress)>(I2C_IMAGE_START_ADDRESS-24))
//	BK_3.BK3status.ReadAddress;
	osDelay(1);

	osDelay(30);

		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
#endif
}
#endif
#if TOK
//---------------------------------------------------------------------------------
static uint32_t C1=0 , D;
void DebugSendUsartMessage(char * str){
	uint8_t B[35];
	D=CO - C1;
	C1=CO;
	sprintf(B,"%5u  ",D);
	sprintf( & B[7],"%s",str);
	xQueueSendToBack(Ququ_Uart, & B,0);
}
//---------------------------------------------------------------------------------
void vTask_EMULATION_1(void const * argument){
	BK_3L *d=(BK_3L *)argument;
// задача производит тестирование алгоритма перегрузки и сверхтока

//------------включение первоначальное------
	osDelay(1000);
	d->test.Tok_EffectValue=5.23;
	osDelay(5000);
	d->test.Tok_EffectValue=7.11;
	osDelay(5000);
	d->test.Tok_EffectValue=14;
	osDelay(25000);
//	ProcSetAvost1();
	osDelay(20000);
//	ProcSetAvost1();
//	osDelay(25000);
	while(1){
//------------ждем 5 сек--------------------
		ProcSetAvost1();
		OtkluchenieWaitGoodVkluchenia(d);
		osDelay(34000);

//------------------конец-----------------

		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
}
#endif

#if OBRIV
void vTask_EMULATION(void const * argument){
	BK_3L *d=(BK_3L *)argument;
// задача производит тестирование алгоритма обрыва реле при отсутствии пропадания фаз
//  поставим все фазы при включении

//------------включение первоначальное------
	d->test.F1_EffectValue=229.;
	d->test.F2_EffectValue=225.;
	d->test.F3_EffectValue=220.;
	d->test.Tok_EffectValue=0;
	osDelay(40);
	OtkluchenieWaitObrivRele(d);

	while(1){
//------------ждем 5 сек--------------------
		osDelay(35000);
//------------------конец-----------------
		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
}
#endif

#if SVAR
void vTask_EMULATION(void const * argument){
	BK_3L *d=(BK_3L *)argument;
// задача производит тестирование алгоритма сваривания реле при отсутствии пропадания фаз
//  поставим все фазы при включении
//------------включение первоначальное------
	d->test.F1_EffectValue=229.;
	d->test.F2_EffectValue=225.;
	d->test.F3_EffectValue=220.;
	d->test.Tok_EffectValue=0;

	OtkluchenieWaitGoodVkluchenia(d);

	while(1){
//------------ждем 5 сек--------------------
		osDelay(5000);
//-------------уберем фазу 1--------------
		d->test.F1_EffectValue=0;
		d->test.F2_EffectValue=220.;
		d->test.F3_EffectValue=223.;
		OtkluchenieWithSvarivanie(d);

		osDelay(20);
//------------ждем 300 сек--------------------
		osDelay(60000);
		OtkluchenieWaitGoodVkluchenia(d);
//------------------конец-----------------
		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
}
#endif
//---------------------------------------------------------------------------------
#if NORM_W
void vTask_EMULATION(void const * argument){
	BK_3L *d=(BK_3L *)argument;
// задача производит поочередное пропадание фаз при нормальном переключении
//  поставим все фазы при включении
	d->test.F1_EffectValue=229.;
	d->test.F2_EffectValue=225.;
	d->test.F3_EffectValue=220.;
	d->test.Tok_EffectValue=0;

	OtkluchenieWaitGoodVkluchenia(d);

	while(1){
//------------ждем 5 сек--------------------
		osDelay(5000);
//-------------уберем фазу 1--------------
		d->test.F1_EffectValue=0;
		d->test.F2_EffectValue=220.;
		d->test.F3_EffectValue=223.;

		OtkluchenieWaitGoodVkluchenia(d);
		osDelay(20);
//------------ждем 5 сек--------------------
		osDelay(5000);
//-------------уберем фазу 2--------------
		d->test.F2_EffectValue=0.;

		OtkluchenieWaitGoodVkluchenia(d);
//------------ждем 5 сек--------------------
		osDelay(5000);
//------------вернем фазы 1 2--------------
		d->test.F2_EffectValue=223.;
		d->test.F1_EffectValue=224.;
//------------ждем 5 сек--------------------
		osDelay(5000);
//-------------уберем фазу 3--------------
		d->test.F3_EffectValue=0;

		OtkluchenieWaitGoodVkluchenia(d);
		osDelay(5000);
//------------------конец-----------------
		BK_3.Stek_t.Emul1Stek=uxTaskGetStackHighWaterMark( NULL);
	}
}
#endif
//---------------------------------------------------------------------------------
static void OtkluchenieWaitGoodVkluchenia(BK_3L *d){
	d->test.OS_EffectValue=0;

	while(d->BK3status.State != WAITING_VKLUCHENIA){
		osDelay(15);
	}
	d->test.OS_EffectValue=220.;
}
//---------------------------------------------------------------------------------
static void OtkluchenieWaitObrivRele(BK_3L *d){
	d->test.OS_EffectValue=0;

	while(d->BK3status.State != WAITING_VKLUCHENIA){
		osDelay(15);
	}
	osDelay(13300);
	d->test.OS_EffectValue=220.;
}
//---------------------------------------------------------------------------------
static void OtkluchenieWithSvarivanie(BK_3L *d){
	d->BK3status.State=NADO_PEREKLUCHAT6;
}

#endif
