#include "hardware_init.h"

uint32_t MASSIV_1_F1[64] , MASSIV_2_F1[64] , MASSIV_1_F2[64] , MASSIV_2_F2[64] , MASSIV_1_F3[64] ,
		MASSIV_2_F3[64];
uint32_t MASSIV_1_OS[64] , MASSIV_2_OS[64] , MASSIV_1_TOK[64] , MASSIV_2_TOK[64] , REZ[64];

uint32_t * P_F1;
uint32_t * P_F2;
uint32_t * P_F3;
uint32_t * P_OS;
uint32_t * P_TOK;

TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim2;
extern I2C_HandleTypeDef hi2c1;
ADC_HandleTypeDef hadc2;
ADC_HandleTypeDef hadc1;

//===============================================================================================
void ADC_Init(void){
	ADC_AnalogWDGConfTypeDef AnalogWDGConfig;
	ADC_ChannelConfTypeDef sConfig;
	ADC_InjectionConfTypeDef sConfigInjected;

	P_F1=MASSIV_1_F1;
	P_F2=MASSIV_1_F2;
	P_F3=MASSIV_1_F3;
	P_OS=MASSIV_1_OS;
	P_TOK=MASSIV_1_TOK;

	hadc2.Instance= ADC2;
	hadc2.Init.ScanConvMode= ADC_SCAN_ENABLE;
	hadc2.Init.ContinuousConvMode=DISABLE;
	hadc2.Init.DiscontinuousConvMode=DISABLE;
	hadc2.Init.ExternalTrigConv= ADC_SOFTWARE_START;
	hadc2.Init.DataAlign= ADC_DATAALIGN_RIGHT;
	hadc2.Init.NbrOfConversion=0;

	HAL_ADC_Init( & hadc2);

	/*  ------------------Configure Injected Channel------------------- */
	sConfigInjected.InjectedChannel= ADC_CHANNEL_2;
	sConfigInjected.InjectedRank=1;
	sConfigInjected.InjectedNbrOfConversion=4;
	sConfigInjected.InjectedSamplingTime= ADC_SAMPLETIME_7CYCLES_5;
	sConfigInjected.ExternalTrigInjecConv= ADC_INJECTED_SOFTWARE_START;
	sConfigInjected.AutoInjectedConv=DISABLE;
	sConfigInjected.InjectedDiscontinuousConvMode=DISABLE;
	sConfigInjected.InjectedOffset=0;
	HAL_ADCEx_InjectedConfigChannel( & hadc2, & sConfigInjected);

	sConfigInjected.InjectedChannel= ADC_CHANNEL_1;
	sConfigInjected.InjectedRank=2;
	HAL_ADCEx_InjectedConfigChannel( & hadc2, & sConfigInjected);

	sConfigInjected.InjectedChannel= ADC_CHANNEL_3;
	sConfigInjected.InjectedRank=3;
	HAL_ADCEx_InjectedConfigChannel( & hadc2, & sConfigInjected);

	sConfigInjected.InjectedChannel= ADC_CHANNEL_8;
	sConfigInjected.InjectedRank=4;
	HAL_ADCEx_InjectedConfigChannel( & hadc2, & sConfigInjected);

	/*========== TIM4 init для тактирования основного процесса=============*/
	TIM_ClockConfigTypeDef sClockSourceConfig;
	TIM_MasterConfigTypeDef sMasterConfig;

	/* TIM4 -   50*64 =3200  */

	uint32_t volatile E=HAL_RCC_GetSysClockFreq();
	uint32_t DIV=( RCC->CFGR & RCC_CFGR_PPRE1);
	E=HAL_RCC_GetPCLK1Freq();

	htim4.Init.Prescaler=((E << 1) / (320000)) - 1;
	htim2.Init.Prescaler=((E << 1) / (10000)) - 1;

//	htim4.Init.Prescaler=719;
	htim4.Instance= TIM4;
	htim4.Init.CounterMode= TIM_COUNTERMODE_UP;
	htim4.Init.Period=100 - 1;
	htim4.Init.ClockDivision= TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init( & htim4);

	sClockSourceConfig.ClockSource= TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource( & htim4, & sClockSourceConfig);

	/*     ADC   */

	sMasterConfig.MasterOutputTrigger= TIM_TRGO_UPDATE;
	sMasterConfig.MasterSlaveMode= TIM_MASTERSLAVEMODE_DISABLE;
	HAL_TIMEx_MasterConfigSynchronization( & htim4, & sMasterConfig);

	HAL_TIM_Base_Start_IT( & htim4);

	/*===============TIM3===========*/

	htim2.Instance= TIM2;

	htim2.Init.CounterMode= TIM_COUNTERMODE_UP;
	htim2.Init.Period=0xFFFF;
	htim2.Init.ClockDivision= TIM_CLOCKDIVISION_DIV1;
	HAL_TIM_Base_Init( & htim2);

	sClockSourceConfig.ClockSource= TIM_CLOCKSOURCE_INTERNAL;
	HAL_TIM_ConfigClockSource( & htim2, & sClockSourceConfig);
	/* выходной триггер для управления ADC */
//	sMasterConfig.MasterOutputTrigger= TIM_TRGO_UPDATE;
//	sMasterConfig.MasterSlaveMode= TIM_MASTERSLAVEMODE_DISABLE;
//	HAL_TIMEx_MasterConfigSynchronization( & htim2, & sMasterConfig);
	HAL_TIM_Base_Start_IT( & htim2);
	/*===================================================*/

	HAL_NVIC_SetPriority(ADC1_2_IRQn,6,0);
	HAL_NVIC_EnableIRQ(ADC1_2_IRQn);

	/*=======================ADC1==========*/

	hadc1.Instance= ADC1;
	hadc1.Init.ScanConvMode= ADC_SCAN_ENABLE;
	hadc1.Init.ContinuousConvMode=ENABLE;
	hadc1.Init.DiscontinuousConvMode=DISABLE;
	hadc1.Init.ExternalTrigConv= ADC_SOFTWARE_START;
	hadc1.Init.DataAlign= ADC_DATAALIGN_RIGHT;
	hadc1.Init.NbrOfConversion=1;
	HAL_ADC_Init( & hadc1);

	/*===================Configure Regular Channel =============*/

	sConfig.Channel= ADC_CHANNEL_9;
	sConfig.Rank=1;
	sConfig.SamplingTime= ADC_SAMPLETIME_7CYCLES_5;
	HAL_ADC_ConfigChannel( & hadc1, & sConfig);
	HAL_ADC_Start( & hadc1);

	/*====================== Watchdog ==========================*/
	AnalogWDGConfig.Channel=9;
	AnalogWDGConfig.HighThreshold= HIGH_LEVEL_ANALOG_WATCHDOG;
	AnalogWDGConfig.ITMode=DISABLE;
	AnalogWDGConfig.LowThreshold= LOW_LEVEL_ANALOG_WATCHDOG;
	AnalogWDGConfig.WatchdogMode= ADC_ANALOGWATCHDOG_SINGLE_REG;
	AnalogWDGConfig.WatchdogNumber=1;

	HAL_ADC_AnalogWDGConfig( & hadc1, & AnalogWDGConfig);
	__HAL_ADC_ENABLE_IT( & hadc1,ADC_IT_AWD);

}
//=========================================================================================================================

