/*
 * BK_3el.h
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */

#ifndef INC_BK_3EL_H_
#define INC_BK_3EL_H_

#include "stm32f1xx_hal.h"
#include "main.h"
#include "freertos_1.h"
#include "math.h"
#include "stm32_dsp.h"
#include "main_proc.h"
#include "hardware_init_2.h"
#include "Indikacia.h"
#include "UsartTask.h"
#include "FlashTask.h"
#include "EmulationTask.h"
#include "settings.h"
#include "I2C_MemDriver.h"
#include "rtc.h"
#include "handler.h"
#endif /* INC_BK_3EL_H_ */
