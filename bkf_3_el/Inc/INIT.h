/*
 * INIT.h
 *
 *  Created on: 24 янв. 2016 г.
 *      Author: Viktor
 */

#ifndef INC_INIT_H_
#define INC_INIT_H_

#include "stm32f1xx_hal.h"
#include "hardware_init.h"
#include "flash_page.h"

#define FLASH_IMAGE_START_ADDR   ADDR_FLASH_PAGE_60                      /* Start @ of user Flash area */
#define FLASH_IMAGE_END_ADDR     ADDR_FLASH_PAGE_125 + FLASH_PAGE_SIZE   /* End @ of user Flash area */

#define FLASH_BOOT_START_ADDR   ADDR_FLASH_PAGE_0                     /* Start @ of user Flash area */
#define FLASH_BOOT_END_ADDR     ADDR_FLASH_PAGE_4 + FLASH_PAGE_SIZE   /* End @ of user Flash area */

//-------------------------------------------------------------------------------------------------------------
typedef struct {
	uint32_t update;					// флаг обновления
	uint32_t size;						// размер новой прошивки
	uint32_t crc32;						// crc 32 новой прошивки
	uint8_t version;
} FIRMWARE_T;
//-------------------------------------------------------------------------------------------------------------
typedef enum {
	NADO_PEREKLUCH, NACH_VKL_1, NACH_VKL_2, NACH_VKL_3, WAIT_VKL, NORM_WRK0, NORM_WRK1, NORM_WRK2, NORM_WRK3,
	NE_NADO_PEREKLUCH, NORMUL_WRK, PEREGR, SVAR1, SVAR2, SVAR3, OBR1, OBR2, OBR3, OBRVOSST, SVERHTOK1, KAV1, /* сообщение о первом Avost */
	KAV2, /* сообщение о втором Avost */
	KAV3, /* сообщение о третьем Avost */
	KAV4, STMFAIL, STMOK, STSFAIL, STSOK, LSFAIL, LSOK, FF10, FF11, FF12, FF13, FF20, FF21, FF22, FF23,

} CMD_UART_T;
//-------------------------------------------------------------------------------------------------------------
typedef enum {
	IND_UI=0, Perekluchenie, Peregruzka, Avaria_faz, Sverhtok, ZALIP1, ZALIP2, ZALIP3, OBR_R_1, OBR_R_2,
	OBR_R_3,

} CMD_INDIKACII_T;
//-------------------------------------------------------------------------------------------------------------
typedef enum {
	store_moment_1=0, store_moment_2, store_moment_3, store_setting, load_setting, MSG_BOOT_OK_FLASH,
	update_setting, MSG_IMAGE_OK_FLASH,

} CMD_FLASH_T;
//-------------------------------------------------------------------------------------------------------------
typedef enum {
	NOINIT=0, INIT_OK, NORMAL_INDIK, PEREKLUCHENIE, PEREGRUZKA, AVARIA_FAZ, SVERHTOK,
} LCD_STATUS_T;
//-------------------------------------------------------------------------------------------------------------
typedef struct {
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef sDate;
} DATATIME_T;
//-------------------------------------------------------------------------------------------------------------
void INDIKACIA_T_Task(void const * argument);
void FLASH_T_Task(void const * argument);
void MX_TIM1_Init(void);
void MX_TIM2_Init(void);
void MX_TIM3_Init(void);
void MX_TIM4_Init(void);
void MX_RTC_Init(void);
void TRANSFER_SETTING_PACKET(void);
void FILL_SETTING_PACKET(void);
void MX_USB_DEVICE_Init(void);
void HAL_ADC_LevelOutOfWindowCallback(ADC_HandleTypeDef* hadc);

#endif /* INC_INIT_H_ */
