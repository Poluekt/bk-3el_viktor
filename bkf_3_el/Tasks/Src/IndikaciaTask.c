/*
 * Indikacia.c
 *
 *  Created on: 29 ���. 2017 �.
 *      Author: Poluect
 */
#include "BK_3el.h"

extern xQueueHandle Ququ_Uart;
extern xQueueHandle Ququ_indik;    // messages to indikacia
extern BK_3L BK_3;

LCD_STATUS_T StatusLcd=NORMAL_INDIK;

uint8_t volatile static NF , OF;
static unsigned char CouComm=0;
static unsigned char CouPeregruz=0;
static unsigned char CouAvarii=0;
static unsigned char CouSverhtok=0;
static unsigned char CouObriv=0;

//============================================================================================================
void IndikaciaTokNapr(void){
	char str[20];
	unsigned int a , b;
	static int A1;

	if(BK_3.F1.StatusFaz.Indikat_value < 5.) BK_3.F1.StatusFaz.Indikat_value=0.;
	if(BK_3.F2.StatusFaz.Indikat_value < 5.) BK_3.F2.StatusFaz.Indikat_value=0.;
	if(BK_3.F3.StatusFaz.Indikat_value < 5.) BK_3.F3.StatusFaz.Indikat_value=0.;

	sprintf(str,"%03u",(unsigned int)BK_3.F1.StatusFaz.Indikat_value);
	sprintf( & str[3],"%s","B ");
	sprintf( & str[5],"%03u",(unsigned int)BK_3.F2.StatusFaz.Indikat_value);
	sprintf( & str[8],"%s","B ");
	sprintf( & str[10],"%03u",(unsigned int)BK_3.F3.StatusFaz.Indikat_value);
	sprintf( & str[13],"%s","B  ");

	WriteStrToLcd(1,0,str);

	a=(unsigned int)BK_3.F_TOK.StatusFaz.Indikat_value;
	b=(unsigned int)((BK_3.F_TOK.StatusFaz.Indikat_value * 10) - (10. * a));

	sprintf(str,"%02u",a);
	sprintf( & str[2],"%s",".");
	sprintf( & str[3],"%01u",b);
	sprintf( & str[4],"%s","A ");

	if(str[0] == 0x30) str[0]=0x20;
//----------------------------------------------------------------------------------
	if(StatusLcd == NORMAL_INDIK){
		if(BK_3.NumerWorkingFase == 1) sprintf( & str[6],"%s","���� �    ");
		if(BK_3.NumerWorkingFase == 2) sprintf( & str[6],"%s","���� �    ");
		if(BK_3.NumerWorkingFase == 3) sprintf( & str[6],"%s","���� �    ");
		if(BK_3.NumerWorkingFase == 0) sprintf( & str[6],"%s","��������� ");
	}
//----------------------------------------------------------------------------------
	if(StatusLcd == PEREKLUCHENIE){
		CouComm++;
		sprintf( & str[6],"%s","����������");

		if(CouComm >= 5){
			CouComm=0;
			StatusLcd=NORMAL_INDIK;
		}
	}else{
		CouComm=0;
	}
////----------------------------------------------------------------------------------
	if(StatusLcd == PEREGRUZKA){
		CouPeregruz++;
		sprintf( & str[6],"%s","����������");

		if(CouPeregruz >= 8){
			CouPeregruz=0;
			StatusLcd=NORMAL_INDIK;
		}
	}else{
		CouPeregruz=0;
	}
////----------------------------------------------------------------------------------
	if(StatusLcd == AVARIA_FAZ){
		if(NF == 1) sprintf( & str[6],"%s","������ 1�  ");
		if(NF == 2) sprintf( & str[6],"%s","������ 2�  ");
		if(NF == 3) sprintf( & str[6],"%s","������ 3�  ");
		CouAvarii++;
		if(CouAvarii >= 9){
			CouAvarii=0;
			StatusLcd=NORMAL_INDIK;
		}
	}else{
		CouAvarii=0;
	}
////----------------------------------------------------------------------------------Norm
	if(StatusLcd == OBRIV_RELE){
		if(OF == 1) sprintf( & str[6],"%s","����� 1�   ");
		if(OF == 2) sprintf( & str[6],"%s","����� 2�   ");
		if(OF == 3) sprintf( & str[6],"%s","����� 3�   ");
		CouObriv++;
		if(CouObriv >= 9){
			CouObriv=0;
			StatusLcd=NORMAL_INDIK;
		}
	}else{
		CouObriv=0;
	}
////----------------------------------------------------------------------------------
	if(StatusLcd == SVERHTOK){
		CouSverhtok++;
		sprintf( & str[6],"%s","��������  ");
		if(BK_3.BK3status.State != STOP_JOB){
			StatusLcd=NORMAL_INDIK;
		}
	}
//----------------------------------------------------------------------------------
	WriteStrToLcd(2,0,str);
//----------------------------------------------------------------------------------
}
//============================================================================================================
void IndikaciaTokVoltage(void){
// �������� ������� ��� ��������� UI 3 ���� � ��� --------------------------------------------------------------
	unsigned char static c;
	unsigned char static LedCou;
//	static CMD_INDIKACII_T a=0;
	static uint32_t ChetchikPeriodov;
	static uint32_t MetkaPeregruzki;
	static const uint16_t PeriodMessagaPeregruzki=10 * 50; // V PERIODAX  - ������ ������� ��������� � ����������

//--------------3���� � ��� �������� ������� ��� ��������� ���� � ����������------------------------------------
	c++;
	if(c >= 17){
		SendMessageToIndikacia(IND_UI);
		c=0;
	}
//--------------------CHETCHIK_PERIODOV 50HZ---------------------------------------------------------------------
	ChetchikPeriodov++;
	if(ChetchikPeriodov == 0xFFFFFFFF){
		MetkaPeregruzki=0;
		ChetchikPeriodov=0;
	}
//-------------�������� �� ���������� �� ���� --------------------------------------------------------------------

	if(BK_3.F_TOK.StatusFaz.Effecient_value > (BK_3.F_TOK.SettingFaz.Max_value * 1.1)){
//----------���������� ------------------------------
//----------��������� �� ����� �� �������� ������� � ����������-------------------------------

		if(ChetchikPeriodov > (MetkaPeregruzki + PeriodMessagaPeregruzki)){
			MetkaPeregruzki=ChetchikPeriodov;
			SendMessageToIndikacia(Peregruzka);
		}
	}
	if(LedCou > 40){
		LED1_TOGGLE;
		LedCou=0;
	}
	LedCou++;
}
//============================================================================================================
void vTask_INDIKACIA(void const * argument){
	CMD_INDIKACII_T send;
	unsigned int volatile I , A , J;
	//   IND_UI �������� 3 ���� � ���
	while(1){
		xQueueReceive(Ququ_indik, & send,portMAX_DELAY);
		A=xPortGetFreeHeapSize();

		switch(send){
			case IND_UI:         //  ----  �������� 3 ���� � ��� ------
				IndikaciaTokNapr();
			break;
			case Perekluchenie:         //  ---- �������� � ������ ��������� ����
				StatusLcd=PEREKLUCHENIE;
				CouComm=0;
			break;
			case Peregruzka:
				StatusLcd=PEREGRUZKA;
				DebugSendUsartMessage("PEREGRUZKA\r\n");
				CouPeregruz=0;
			break;
			case Avaria_faz:
				StatusLcd=AVARIA_FAZ;
				CouAvarii=0;
			break;
			case Sverhtok:
				StatusLcd=SVERHTOK;
				CouSverhtok=0;
			break;
			case ZALIP1:
				StatusLcd=AVARIA_FAZ;
				CouAvarii=0;
				NF=1;
			break;
			case ZALIP2:
				StatusLcd=AVARIA_FAZ;
				CouAvarii=0;
				NF=2;
			break;
			case ZALIP3:
				StatusLcd=AVARIA_FAZ;
				CouAvarii=0;
				NF=3;
			break;
			case OBR_R_1:
				StatusLcd=OBRIV_RELE;
				CouObriv=0;
				OF=1;
			break;
			case OBR_R_2:
				StatusLcd=OBRIV_RELE;
				CouObriv=0;
				OF=2;
			break;
			case OBR_R_3:
				StatusLcd=OBRIV_RELE;
				CouObriv=0;
				OF=3;
			break;
		}
		BK_3.Stek_t.IndTaskStek=uxTaskGetStackHighWaterMark( NULL);
		vTaskDelay(200);
	}
}
//============================================================================================================
