/**
 ******************************************************************************
 * @file    stm32f1xx_it.c
 * @brief   Interrupt Service Routines.
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"
#include "stm32f1xx.h"
#include "stm32f1xx_it.h"
#include "cmsis_os.h"
//#include "INIT.h"
extern PCD_HandleTypeDef hpcd_USB_FS;
TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern RTC_HandleTypeDef hrtc;
extern ADC_HandleTypeDef hadc2;
extern ADC_HandleTypeDef hadc1;
extern void USART3_portserial_IRQHandler(void);
void prvGetRegistersFromStack(uint32_t *pulFaultStackAddress);
void NMI_Handler(void){

}
static void HardFault_Handler(void)__attribute__( ( naked ) );
static void HardFault_Handler(void){
	__asm volatile
	(
			" tst lr, #4                                                \n"
			" ite eq                                                    \n"
			" mrseq r0, msp                                             \n"
			" mrsne r0, psp                                             \n"
			" ldr r1, [r0, #24]                                         \n"
			" ldr r2, handler2_address_const                            \n"
			" bx r2                                                     \n"
			" handler2_address_const: .word prvGetRegistersFromStack    \n"
	);
}

void prvGetRegistersFromStack(uint32_t *pulFaultStackAddress){
	/* These are volatile to try and prevent the compiler/linker optimising them
	 away as the variables never actually get used.  If the debugger won't show the
	 values of the variables, make them global my moving their declaration outside
	 of this function. */
	volatile uint32_t r0;
	volatile uint32_t r1;
	volatile uint32_t r2;
	volatile uint32_t r3;
	volatile uint32_t r12;
	volatile uint32_t lr; /* Link register. */
	volatile uint32_t pc; /* Program counter. */
	volatile uint32_t psr;/* Program status register. */

	r0 = pulFaultStackAddress[0];
	r1 = pulFaultStackAddress[1];
	r2 = pulFaultStackAddress[2];
	r3 = pulFaultStackAddress[3];

	r12 = pulFaultStackAddress[4];
	lr = pulFaultStackAddress[5];
	pc = pulFaultStackAddress[6];
	psr = pulFaultStackAddress[7];

	/* When the following line is hit, the variables contain the register values. */
	for (;;)
		;
}
void prvGetRegistersFromStack2(uint32_t *pulFaultStackAddress){
	/* These are volatile to try and prevent the compiler/linker optimising them
	 away as the variables never actually get used.  If the debugger won't show the
	 values of the variables, make them global my moving their declaration outside
	 of this function. */
	volatile uint32_t r0;
	volatile uint32_t r1;
	volatile uint32_t r2;
	volatile uint32_t r3;
	volatile uint32_t r12;
	volatile uint32_t lr; /* Link register. */
	volatile uint32_t pc; /* Program counter. */
	volatile uint32_t psr;/* Program status register. */

	r0 = pulFaultStackAddress[0];
	r1 = pulFaultStackAddress[1];
	r2 = pulFaultStackAddress[2];
	r3 = pulFaultStackAddress[3];

	r12 = pulFaultStackAddress[4];
	lr = pulFaultStackAddress[5];
	pc = pulFaultStackAddress[6];
	psr = pulFaultStackAddress[7];

	/* When the following line is hit, the variables contain the register values. */
	for (;;)
		;
}

void MemManage_Handler(void){

	while(1){
	}

}
void BusFault_Handler(void)__attribute__( ( naked ) );
void BusFault_Handler(void){
//	uint32_t NN;
//	NN=SCB->BFAR;
	__asm volatile
	(
			" tst lr, #4                                                \n"
			" ite eq                                                    \n"
			" mrseq r0, msp                                             \n"
			" mrsne r0, psp                                             \n"
			" ldr r1, [r0, #24]                                         \n"
			" ldr r2, handler3_address_const                            \n"
			" bx r2                                                     \n"
			" handler3_address_const: .word prvGetRegistersFromStack2    \n"
	);
	while(1){
	}

}

/**
 * @brief This function handles Undefined instruction or illegal state.
 */
void UsageFault_Handler(void){

	while(1){
	}

}
void DebugMon_Handler(void){

	while(1){
	}

}

//--------------------------------------------------------------
void SysTick_Handler(void){

	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();
	osSystickHandler();

}
//--------------------------------------------------------------
void USB_LP_CAN1_RX0_IRQHandler(void){
	HAL_PCD_IRQHandler(&hpcd_USB_FS);
}
//--------------------------------------------------------------
void TIM4_IRQHandler(void){

	if(__HAL_TIM_GET_FLAG(&htim4, TIM_FLAG_UPDATE) != RESET){
		if(__HAL_TIM_GET_IT_SOURCE(&htim4, TIM_IT_UPDATE) != RESET){
			__HAL_TIM_CLEAR_IT(&htim4, TIM_IT_UPDATE);
			HAL_TIM_PeriodElapsedCallback(&htim4);

		}
	}
}
////--------------------------------------------------------------
void TIM3_IRQHandler(void){
	HAL_TIM_IRQHandler(&htim3);
}
//--------------------------------------------------------------
void RTC_IRQHandler(void){

	HAL_RTCEx_RTCIRQHandler(&hrtc);

}
////--------------------------------------------------------------
void RTC_Alarm_IRQHandler(void){

	HAL_RTC_AlarmIRQHandler(&hrtc);

}
////--------------------------------------------------------------
void ADC1_2_IRQHandler(void){
	signed int volatile static B, E, D;

	B = (signed int) TIM2->CNT;
// JEOC
	if(__HAL_ADC_GET_IT_SOURCE( & hadc2 , ADC_IT_JEOC ) && (__HAL_ADC_GET_FLAG(&hadc2, ADC_FLAG_JEOC))){
		HAL_ADC_IRQHandler(&hadc2);
	}
// AWD
	else
		if(__HAL_ADC_GET_IT_SOURCE( & hadc1 , ADC_IT_AWD ) && (__HAL_ADC_GET_FLAG(&hadc1, ADC_SR_AWD))){
			HAL_ADC_IRQHandler(&hadc2);
		}
//EOC
		else
			if(__HAL_ADC_GET_IT_SOURCE( & hadc1 , ADC_IT_EOC ) && (__HAL_ADC_GET_FLAG(&hadc1, ADC_FLAG_EOC))){
				HAL_ADC_IRQHandler(&hadc1);
			}
			else
// ELSE
			{
				HAL_ADC_IRQHandler(&hadc1);
			}

	E = (signed int) TIM2->CNT;
	D = E - B;
	if(D > 500){
		D = 500;
	}
}
//--------------------------------------------------------------
void USART3_IRQHandler(void){

	USART3_portserial_IRQHandler();

}
//--------------------------------------------------------------
void EXTI0_IRQHandler(void){

	HAL_GPIO_EXTI_IRQHandler(GPIO_PIN_0);

}
//--------------------------------------------------------------
