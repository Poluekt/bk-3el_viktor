#include "BK_3el.h"

#ifdef USART_DEBUG

extern xQueueHandle Ququ_Uart;
extern xQueueHandle Ququ_indik;
extern BK_3L BK_3;
extern UART_HandleTypeDef huart3;
/**
 * @brief
 */
//--------------------------------------------------------------------------------------
void vTask_UART(void const * argument){
	uint8_t send;
	unsigned int volatile I , A , J;
	char str[25];
	while(1){
		xQueueReceive(Ququ_Uart, & str,portMAX_DELAY);
		A=xPortGetFreeHeapSize();

		Usart3SendMessageOnline(str);
		osDelay(180);
		BK_3.Stek_t.UartTaskStek=uxTaskGetStackHighWaterMark( NULL);
	}
}
//--------------------------------------------------------------------------------------
/**
 * @brief
 */
void Usart3SendMessageOnline(char * MESSAGE){
	uint16_t Size;
	char * P;
	P=MESSAGE;
	Size=0;
	while((( * P++) != 0) && (Size < 3000)){
		Size++;
	}
	USART3_RW_SET_1;
	HAL_UART_Transmit( & huart3,(uint8_t *)MESSAGE,Size,50);
	USART3_RW_SET_0;
}
//--------------------------------------------------------------------------------------
#endif
