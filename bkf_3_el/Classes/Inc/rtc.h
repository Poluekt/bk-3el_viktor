/*
 * rtc.h
 *
 *  Created on: 17 нояб. 2017 г.
 *      Author: Poluect
 */

#ifndef CLASSES_INC_RTC_H_
#define CLASSES_INC_RTC_H_

#include "stm32f1xx_hal.h"
#include "INIT.h"

void MX_RTC_Init(void);
void PackDateToUpdate(void);
void GetDateToUpdate(void);
void RTC_GetDate(RTC_DateTypeDef *sDate);
void RTC_GetDateTime(DATATIME_T *dt);
void RTC_GetTime(RTC_TimeTypeDef *sTime);
void RTC_SetDateTime(DATATIME_T *dt);

#endif /* CLASSES_INC_RTC_H_ */
