/*
 * hardware_inyt_2.c

 *
 *  Created on: 30 июня 2016 г.
 *      Author: Viktor
 */
#include "BK_3el.h"

extern BK_3L BK_3;

UART_HandleTypeDef huart3;
RTC_HandleTypeDef hrtc;
void PackDateToUpdate(void);
static void GpioInit(void);
//================================================================================================
void device_init(void){
	eMBErrorCode eStatus;
	uint8_t L;

	GpioInit();
	StructuresInit();
	I2C_Init();
	WC_L;
	ADC_Init();
	RelayInit();
	MX_RTC_Init();
	Otkluchit6_all();
	eStatus=eMBInit(MB_RTU,0x0A,0,9600,MB_PAR_NONE);
	HAL_NVIC_SetPriority(EXTI0_IRQn,0,0);
	HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	LcdInit();
	osDelay(10);
	WriteStrToLcd(1,0,"������");
	osDelay(10);
	eMBEnable();
	MX_USB_DEVICE_Init();
	InitPointers();
}
//=====================================================================================================
static void GpioInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;

	__GPIOB_CLK_ENABLE()
	;
	__GPIOA_CLK_ENABLE()
	;
	__GPIOC_CLK_ENABLE()
	;
#if 0

	/*LCD  */
	GPIO_InitStruct.Pin= RW_Pin;

	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_OD;
	GPIO_InitStruct.Speed= GPIO_SPEED_HIGH;
	HAL_GPIO_Init( RW_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= E_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_HIGH;
	HAL_GPIO_Init( E_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= RS_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_HIGH;
	HAL_GPIO_Init( RS_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= GPIO_PIN_6 | GPIO_PIN_7;
	GPIO_InitStruct.Mode= GPIO_MODE_AF_OD;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB4_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_HIGH;
	HAL_GPIO_Init( DB4_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB5_Pin;
	HAL_GPIO_Init( DB5_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB6_Pin;
	HAL_GPIO_Init( DB6_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB7_Pin;
	HAL_GPIO_Init( DB7_GPIO_Port, & GPIO_InitStruct);

	/*LIGHT  */
	GPIO_InitStruct.Pin= LIGHT_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(LIGHT_GPIO_PORT, & GPIO_InitStruct);
	/*ADC   */
	GPIO_InitStruct.Pin= ADC_F2_Pin | ADC_F1_Pin | ADC_F3_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_ANALOG;
	HAL_GPIO_Init(GPIOA, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= ADC_OS_Pin | ADC_TOK_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_ANALOG;
	HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);

	/*USART3   */
	GPIO_InitStruct.Pin= USART3_TX_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_AF_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= USART3_RX_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull= GPIO_NOPULL;
	HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= USART3_RW_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull= GPIO_PULLUP;
	HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);

	/*RELAY  */
	GPIO_InitStruct.Pin= RELAY_1_PIN | RELAY_2_PIN | RELAY_3_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init( RELAY_GPIO_PORT, & GPIO_InitStruct);

	/*USB_DISCONNECT_PIN   */
	GPIO_InitStruct.Pin= USB_DISCONNECT_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(USB_DISCONNECT, & GPIO_InitStruct);
	/*WC I2C   */
	GPIO_InitStruct.Pin= GPIO_PIN_9;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(GPIOB, & GPIO_InitStruct);
	/* LED   */
	GPIO_InitStruct.Pin= LED1_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(LED1_GPIO_PORT, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= LED2_PIN;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(LED2_GPIO_PORT, & GPIO_InitStruct);

	__HAL_AFIO_REMAP_SWJ_NOJTAG();

#else
	MODIFY_REG(GPIOA->CRH,0xffffffff,0x38844334);
	MODIFY_REG(GPIOA->CRL,0xffffffff,0x11410007);

	MODIFY_REG(GPIOB->CRH,0xffffffff,0x33434B13);
	MODIFY_REG(GPIOB->CRL,0xffffffff,0xff334100);

	MODIFY_REG(GPIOC->CRH,0xffffffff,0x44144444);
	MODIFY_REG(GPIOC->CRL,0xffffffff,0x44444444);

	MODIFY_REG(AFIO->MAPR,0xffffffff,0x2000000);

#endif
	LED1_OFF;
	LED2_OFF;
	LIGHT_OFF;
	WC_H;
}

//================================================================================================
void RelayInit(void){
	GPIO_InitTypeDef GPIO_InitStruct;

	__HAL_RCC_GPIOA_CLK_ENABLE()
	;

	RELAY_1_OFF;
	RELAY_2_OFF;
	RELAY_3_OFF;
}
//================================================================================================
void StructuresInit(void){

	BK_3.Counter40TFromCommut=0;
	BK_3.CounterTFromSvariv=0;
	BK_3.CounterTFromObriv=0;
	BK_3.CounterTFromAvost=0;
	BK_3.BK3status.State=NACHALNOE_VKLUCHENIE;
	BK_3.NumerWorkingFase=0;
	BK_3.Setting.NumerHighPriorityFase=1;
	BK_3.Setting.NumerMediumPriorityFase=2;
	BK_3.Setting.NumerMLowPriorityFase=3;
	BK_3.NumerFaseForPushUp_1=2;
	BK_3.NumerFaseForPushUp_2=3;
	BK_3.Nomer1=0;
	BK_3.Nomer2=0;
	BK_3.Setting.Politika_nevozmognosti_perekluchenia=Ostavit6_kak_est6;

	BK_3.F1.Numer=1;
	BK_3.F1.Flag_of_presence=1;
	BK_3.F1.Count_of_unpresense=0;
	BK_3.F1.Count_of_presense=50;
	BK_3.F1.StatusFaz.Effecient_value=0.;
	BK_3.F1.StatusFaz.Fase=0;
	BK_3.F1.SettingFaz.Level_of_presence=120.;
	BK_3.F1.SettingFaz.Max_value=260.;
	BK_3.F1.SettingFaz.Min_value=150.;
	BK_3.F1.SettingFaz.Koeff_mashtabirovania=220. / 500. * 206. / 203.;
	BK_3.F1.Flag_of_out_up_max_value=0;
	BK_3.F1.Flag_of_out_down_min_value=0;
	BK_3.F1.Count_of_min_value_flag=0;
	BK_3.F1.Count_of_max_value_flag=0;
	BK_3.F1.Count_of_periodov_of_normal_work=0;
	BK_3.F1.StatusFaz.ConnectStatus=DISCONNECT;

	memcpy( & BK_3.F2, & BK_3.F1,sizeof(BK_3.F1));
	BK_3.F2.Numer=2;
	BK_3.F2.SettingFaz.Koeff_mashtabirovania=220. / 500. * 206. / 203.;

	memcpy( & BK_3.F3, & BK_3.F1,sizeof(BK_3.F1));
	BK_3.F3.Numer=3;
	BK_3.F3.SettingFaz.Koeff_mashtabirovania=220. / 500. * 206. / 203.;

	memcpy( & BK_3.F_OS, & BK_3.F1,sizeof(BK_3.F1));
	BK_3.F_OS.Numer=4;
	BK_3.F_OS.SettingFaz.Koeff_mashtabirovania=220. / 500. * 206. / 203.;

	memcpy( & BK_3.F_TOK, & BK_3.F1,sizeof(BK_3.F1));
	BK_3.F_TOK.Numer=5;
	BK_3.F_TOK.SettingFaz.Koeff_mashtabirovania=220. / 500. * 206. / 203.;
	BK_3.F_TOK.SettingFaz.Level_of_presence=0.1;
	BK_3.F_TOK.SettingFaz.Max_value=12.;
	BK_3.F_TOK.SettingFaz.Min_value=0.01;
	BK_3.F_TOK.SettingFaz.Koeff_mashtabirovania=5. / 2000.;

	BK_3.BK3status.NumerFaseForSwitch=0;

	SendMessageToFlash(load_setting);
}

//================================================================================================

