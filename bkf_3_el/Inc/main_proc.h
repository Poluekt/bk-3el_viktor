/*
 * main_proc.h
 *
 *  Created on: 1 июля 2016 г.
 *      Author: Viktor
 */

#ifndef INC_MAIN_PROC_H_
#define INC_MAIN_PROC_H_

#include "main.h"


void ProcNachalnogoVkluchenia ( void );
void ProcSetAvost1 ( void );
void ProcSetAvost ( void );
void SvarivanieHandler(void);
void ObrivReleHandler(void);
void Stalking(FAZA_t * F);
void Operacia(void);
void StructuresInit(void);
void ProcNormalWork(void);
void FindFaseForPushUp(void);
void ProcWaitingVkluchenia(void);
void ProcWaitingOtkluchenia(void);
void AvostHandler(void);
void StopJobHandler(void);
void ProcSetAvost(void);
void ProcSetAvost(void);
void OPERACIA_3(void);
void Vkluchit6(void);
void FindKandidats(void);
void OtkluchenieHandler(void);
void Otkluchit6_all ( void );
void ObrivHandler(void);
#endif /* INC_MAIN_PROC_H_ */
