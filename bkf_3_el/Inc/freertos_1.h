/*
 * freertos.h
 *
 *  Created on: 3 июля 2016 г.
 *      Author: Viktor
 */

#ifndef INC_FREERTOS_1_H_
#define INC_FREERTOS_1_H_

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "INIT.h"
#include "LCD.h"

void vTask_INDIKACIA ( void const * argument );
void IndikaciaTokVoltage ( void );
void vTask_FLASH ( void  * argument);
void DebugSendUsartMessage(char *  str);
void vTask_EMULATION ( void const * argument );

#endif /* INC_FREERTOS_1_H_ */
