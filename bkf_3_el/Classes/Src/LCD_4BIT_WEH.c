//#include "task.h"
//#include "stm32f1xx_hal.h"
#include "INIT.h"
#include "LCD.h"
#include "cmsis_os.h"

static const uint8_t Z=20;
static uint8_t status_lcd=NOINIT;
uint32_t Cou;

unsigned char const PEREKOD[]={0x41,0xA0,0x42,0xA1,0xE0,0x45,0xA3,0xA4,0xA5,0xA6,0x4B,0xA7,0x4D,0x48,0x4F,0xA8,0x50,
		0x43,0x54,0xA9,0xAA,0x58,0xE1,0xAB,0xAC,0xE2,0xAD,0xAE,0xC4,0xAF,0xB0,0xB1,0x61,0xB2,0xB3,0xB4,0xE3,0x69,0xB6,
		0xB7,0xB8,0xBA,0xBB,0xBC,0xBD,0x6F,0xBE,0x60,0x63,0xBF,0x79,0xE4,0x78,0xE5,0xC0,0xC1,0xE6,0xC2,0xC3,0xC4,0xC5,
		0xC6,0xC7};

static void LcdWrite(unsigned char DAT);
static void WriteLcdAddress(uint8_t ADR);
static void LcdInitOut(void);
static void LcdInitIn(void);
static uint8_t BusyFlag(void);
static uint8_t LcdReadData(void);

//========================================================================================================
void LcdInit(void){
	__HAL_AFIO_REMAP_SWJ_NOJTAG();
	RS_OFF;
	E_OFF;
	RW_ON;

	LIGHT_OFF;
	osDelay(500);
	LIGHT_ON;

	osDelay(500);

	{
	WriteLcdHighNibble(3);
	osDelay(5);
	WriteLcdHighNibble(3);
	osDelay(5);
	WriteLcdHighNibble(3);
	osDelay(5);
	WriteLcdHighNibble(2);
	osDelay(5);
	}

//---------------------------------------------------------
	WriteLcdHighNibble(2);      // FUNCTION SET
	WriteLcdHighNibble(8);
	Cou=0;
	while(BusyFlag()){
		Cou++;
	}
//--------------------------------------------------------
	LcdWriteCmd(0x08);    // DISPLAY ON/OFF CONTROL
	Cou=0;
	while(BusyFlag() && (Cou < 25000)){
		Cou++;
	}
//--------------------------------------------------------
	LcdWriteCmd(0x0C);    // DISPLAY ON/OFF CONTROL
	Cou=0;
	while(BusyFlag() && (Cou < 25000)){
		Cou++;
	}
//--------------------------------------------------------
	LcdWriteCmd(0x01);    // clear
	Cou=0;
	while(BusyFlag() && (Cou < 25000)){
		Cou++;
	}
//--------------------------------------------------------
	LcdWriteCmd(0x06);    // ENTRY MODE SET
	Cou=0;
	while(BusyFlag() && (Cou < 25000)){
		Cou++;
	}
//--------------------------------------------------------
	LcdWriteCmd(2);    // HOME
	Cou=0;
	while(BusyFlag() && (Cou < 25000)){
		Cou++;
	}
	status_lcd=INIT_OK;
}
//================================================================================================================
void WriteStrToLcd(uint8_t LINE_NUMBER,uint8_t POS,char * str){
	//  LINE_NUMBER 1  or  2
	volatile uint8_t A , B;
	uint32_t COU;
	if(status_lcd != INIT_OK) return;
	A=0;
	COU=0;
	while(BusyFlag()){
		COU++;
	}
//--------------------------------------------------------
	if(LINE_NUMBER == 1){
		WriteLcdAddress(POS);
	}else{
		WriteLcdAddress(POS + 0x40);
	}
	COU=0;
	while(BusyFlag()){
		COU++;
	}

	while((( * str) != 0) && (A < 24)){
		B= * str;

		if(B >= 0xC0) B=PEREKOD[B - 0xC0];
		else if(B == 0x01) B=0xDF;

		LcdWriteData(B);
		A++;
		str++;
		COU=0;
		while(BusyFlag()){
			COU++;
		}
	}
}
//================================================================================================================
static void WriteLcdAddress(uint8_t ADR){
	LcdWriteCmd(ADR | 0x80);
}
//================================================================================================================
static uint8_t StatusRg(void){

	RS_OFF;
// -------------------
	LcdDelay(Z);
	LcdInitIn();       //DB7-DB4 - INPUT
	LcdDelay(Z);
// -------------------
	RW_ON;
	LcdDelay(Z);
// -------------------------------------BEGIN READING------------------------------------
	E_ON;
	LcdDelay(Z);
	uint16_t D1=0;
// ----H NIBBLE--------
	if(HAL_GPIO_ReadPin(DB4_GPIO_Port,DB4_Pin)) D1=D1 | 0x10;
	if(HAL_GPIO_ReadPin(DB5_GPIO_Port,DB5_Pin)) D1=D1 | 0x20;
	if(HAL_GPIO_ReadPin(DB6_GPIO_Port,DB6_Pin)) D1=D1 | 0x40;
	if(HAL_GPIO_ReadPin(DB7_GPIO_Port,DB7_Pin)) D1=D1 | 0x80;
	LcdDelay(Z);
	E_OFF;
//--------------------
	LcdDelay(Z);
	E_ON;
	LcdDelay(Z);
//----- L NIBBLE------
	if(HAL_GPIO_ReadPin(DB4_GPIO_Port,DB4_Pin)) D1=D1 | 0x01;
	if(HAL_GPIO_ReadPin(DB5_GPIO_Port,DB5_Pin)) D1=D1 | 0x02;
	if(HAL_GPIO_ReadPin(DB6_GPIO_Port,DB6_Pin)) D1=D1 | 0x04;
	if(HAL_GPIO_ReadPin(DB7_GPIO_Port,DB7_Pin)) D1=D1 | 0x08;
	LcdDelay(Z);
	E_OFF;
	LcdDelay(Z);
//---------------- RETURN DB7-DB4 TO OUT-------------
	LcdInitOut();    //DB7-DB4 - OUTPUT
	LcdDelay(Z);
	return D1;
}
//================================================================================================================
static void LcdInitIn(void){
#if 0
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Pin= DB4_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_INPUT;
	GPIO_InitStruct.Speed= GPIO_SPEED_HIGH;
	HAL_GPIO_Init( DB4_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB5_Pin;
	HAL_GPIO_Init( DB5_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB6_Pin;
	HAL_GPIO_Init( DB6_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB7_Pin;
	HAL_GPIO_Init( DB7_GPIO_Port, & GPIO_InitStruct);

#else
	MODIFY_REG(GPIOA->CRH,0xffffffff,0x88844334);
	MODIFY_REG(GPIOA->CRL,0xffffffff,0x11410007);

	MODIFY_REG(GPIOB->CRH,0xffffffff,0x33434B18);
	MODIFY_REG(GPIOB->CRL,0xffffffff,0xFF884100);


#endif
}
//================================================================================================================
static void LcdInitOut(void){
#if 0
	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.Pin= DB4_Pin;
	GPIO_InitStruct.Mode= GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Speed= GPIO_SPEED_HIGH;
	HAL_GPIO_Init( DB4_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB5_Pin;
	HAL_GPIO_Init( DB5_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB6_Pin;
	HAL_GPIO_Init( DB6_GPIO_Port, & GPIO_InitStruct);

	GPIO_InitStruct.Pin= DB7_Pin;
	HAL_GPIO_Init( DB7_GPIO_Port, & GPIO_InitStruct);

#else
	MODIFY_REG(GPIOA->CRH,0xffffffff,0x38844334);
	MODIFY_REG(GPIOA->CRL,0xffffffff,0x11410007);

	MODIFY_REG(GPIOB->CRH,0xffffffff,0x33434B13);
	MODIFY_REG(GPIOB->CRL,0xffffffff,0xFF334100);
#endif
}
//================================================================================================================
static uint8_t BusyFlag(void){
	if(StatusRg() & 0x80){
		return 1;
	}else{
		return 0;
	}
}
//================================================================================================================
void LcdWriteData(unsigned char DAT){
	RS_ON;
	LcdWrite(DAT);
}
//================================================================================================================
void LcdOff(void){
	LcdWriteCmd(0x08);    // DISPLAY ON/OFF CONTROL
	Cou=0;
	while(BusyFlag() && (Cou < 25000)){
		Cou++;
	}
}
//================================================================================================================
static uint8_t LcdReadData(void){
	uint8_t C;
	uint16_t A , B , D1;
	RS_ON;

	LcdDelay(Z);
	LcdInitIn();       //DB7-DB4 - INPUT
	LcdDelay(Z);
// -------------------
	RW_ON;
	LcdDelay(Z);
// -------------------------------------BEGIN READING----------
	E_ON;
	LcdDelay(Z);
	D1=0;
// ----H NIBBLE-----------

	if(HAL_GPIO_ReadPin(DB4_GPIO_Port,DB4_Pin)) D1=D1 | 0x10;
	if(HAL_GPIO_ReadPin(DB5_GPIO_Port,DB5_Pin)) D1=D1 | 0x20;
	if(HAL_GPIO_ReadPin(DB6_GPIO_Port,DB6_Pin)) D1=D1 | 0x40;
	if(HAL_GPIO_ReadPin(DB7_GPIO_Port,DB7_Pin)) D1=D1 | 0x80;
	LcdDelay(Z);
	E_OFF;
//------------------------------------------------------------
	LcdDelay(Z);
	E_ON;
	LcdDelay(Z);
//----- L NIBBLE----------------------------------------------

	if(HAL_GPIO_ReadPin(DB4_GPIO_Port,DB4_Pin)) D1=D1 | 0x01;
	if(HAL_GPIO_ReadPin(DB5_GPIO_Port,DB5_Pin)) D1=D1 | 0x02;
	if(HAL_GPIO_ReadPin(DB6_GPIO_Port,DB6_Pin)) D1=D1 | 0x04;
	if(HAL_GPIO_ReadPin(DB7_GPIO_Port,DB7_Pin)) D1=D1 | 0x08;
	LcdDelay(Z);
	E_OFF;
	LcdDelay(Z);
//------------------------------------------------------------
	C=A | B;
//---------------- RETURN DB7-DB4 TO OUT----------------------
	LcdInitOut();    //DB7-DB4 - OUTPUT
	RS_OFF;
	LcdDelay(Z);
	return C;
}
//================================================================================================================
static void LcdWrite(unsigned char DAT){
	RW_ON;
	LcdDelay(Z);
//-------------------------------------------------------------------------------------
	HAL_GPIO_WritePin( DB4_GPIO_Port,DB4_Pin,((DAT & 0x10) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB5_GPIO_Port,DB5_Pin,((DAT & 0x20) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB6_GPIO_Port,DB6_Pin,((DAT & 0x40) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB7_GPIO_Port,DB7_Pin,((DAT & 0x80) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);

	LcdDelay(Z);
//-------------------------------------------------------------------------------------
	RW_OFF;
	LcdDelay(Z);
//-------------------------------------------------------------------------------------
	E_ON;
	LcdDelay(Z);
	E_OFF;
//-------------------------------------------------------------------------------------
	LcdDelay(Z);
	RW_ON;
	HAL_GPIO_WritePin( DB4_GPIO_Port,DB4_Pin,((DAT & 0x01) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB5_GPIO_Port,DB5_Pin,((DAT & 0x02) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB6_GPIO_Port,DB6_Pin,((DAT & 0x04) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB7_GPIO_Port,DB7_Pin,((DAT & 0x08) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	LcdDelay(Z);
//-------------------------------------------------------------------------------------
	RW_OFF;
	E_ON;
	LcdDelay(Z);
	E_OFF;
	RW_ON;
//-------------------------------------------------------------------------------------
	LcdDelay(Z);

}
//=========================================================================
void WriteLcdHighNibble(uint8_t NIBBLE){
//-------------------------------------------------------------------------------------
	RS_OFF;
	RW_OFF;
	E_OFF;
//-------------------------------------------------------------------------------------
	LcdDelay(Z);

	HAL_GPIO_WritePin( DB4_GPIO_Port,DB4_Pin,((NIBBLE & 0x01) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB5_GPIO_Port,DB5_Pin,((NIBBLE & 0x02) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB6_GPIO_Port,DB6_Pin,((NIBBLE & 0x04) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);
	HAL_GPIO_WritePin( DB7_GPIO_Port,DB7_Pin,((NIBBLE & 0x08) != 0) ? GPIO_PIN_SET : GPIO_PIN_RESET);

	LcdDelay(Z);
	E_ON;
	LcdDelay(Z);
	E_OFF;
	LcdDelay(Z);
//-------------------------------------------------------------------------------------
	RW_ON;
}
//================================================================================================================
void LcdWriteCmd(unsigned char CMD){
	RS_OFF;
	LcdWrite(CMD);
}
//================================================================================================================
void LcdDelay(uint32_t volatile DELAY){
	uint32_t volatile DELAY1;
	DELAY1=DELAY;
	while(DELAY1--){
		__NOP();
	};
}
//================================================================================================================

