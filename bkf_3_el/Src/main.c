
#include "BK_3el.h"

ADC_HandleTypeDef hadc2;
RTC_HandleTypeDef hrtc;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
UART_HandleTypeDef huart3;

osThreadId MainTaskHandle;
osThreadId FlashTaskHandle;
osThreadId IndikTaskHandle;
osThreadId UartTaskHandle;
osThreadId EMULTaskHandle;
osThreadId EMULTask1Handle;
extern BK_3L BK_3;
xSemaphoreHandle xStartCalculate;
xSemaphoreHandle xStartTest;
xSemaphoreHandle xUsartTest;
xSemaphoreHandle xI2CSemaphore;
xQueueHandle Ququ_flash;
xQueueHandle Ququ_indik;
xQueueHandle Ququ_Uart;

static void SystemClock_Config(void);

//===========================================================================================================
int main(void){
	unsigned int volatile I , A;
	eMBErrorCode eStatus;

	HAL_Init();

	SystemClock_Config();

	SCB->SHCSR|= SCB_SHCSR_BUSFAULTENA;
	SCB->SHCSR|= SCB_SHCSR_MEMFAULTENA;
	SCB->SHCSR|= SCB_SHCSR_USGFAULTENA;

	osThreadDef(defaultTask,(void * ) vTaskMain,osPriorityHigh,0,200);
	MainTaskHandle=osThreadCreate(osThread(defaultTask), & BK_3);

	osThreadDef(FLASHTask,(void * )vTask_FLASH,osPriorityNormal,0,1 * 192);
	FlashTaskHandle=osThreadCreate(osThread(FLASHTask), & BK_3);

#ifdef USART_DEBUG
	osThreadDef(UARTask,vTask_UART,osPriorityBelowNormal,0, 100);
	UartTaskHandle=osThreadCreate(osThread(UARTask), & BK_3);
#endif

	vSemaphoreCreateBinary(xStartCalculate);
	vSemaphoreCreateBinary(xStartTest);
	vSemaphoreCreateBinary(xI2CSemaphore);
	vSemaphoreCreateBinary(xUsartTest);
	A=xPortGetFreeHeapSize();
	Ququ_flash=xQueueCreate(20,2);
	Ququ_indik=xQueueCreate(5,1);
	Ququ_Uart=xQueueCreate(20,35);
	A=xPortGetFreeHeapSize();

	osKernelStart();

	while(1){
	}
}
//=================================================================================================
static void SystemClock_Config(void){
	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	RCC_OscInitStruct.OscillatorType= RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;

	RCC_OscInitStruct.HSEState= RCC_HSE_ON;
	RCC_OscInitStruct.HSEPredivValue= RCC_HSE_PREDIV_DIV1;
	RCC_OscInitStruct.LSEState= RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState= RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource= RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLMUL= RCC_PLL_MUL9;
	HAL_RCC_OscConfig( & RCC_OscInitStruct);

	RCC_ClkInitStruct.ClockType= RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1;
	RCC_ClkInitStruct.SYSCLKSource= RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider= RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider= RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider= RCC_HCLK_DIV1;
	HAL_RCC_ClockConfig( & RCC_ClkInitStruct,FLASH_LATENCY_2);

	PeriphClkInit.PeriphClockSelection= RCC_PERIPHCLK_RTC | RCC_PERIPHCLK_ADC | RCC_PERIPHCLK_USB;

	PeriphClkInit.RTCClockSelection= RCC_RTCCLKSOURCE_LSE;
	PeriphClkInit.AdcClockSelection= RCC_ADCPCLK2_DIV8;
	PeriphClkInit.UsbClockSelection= RCC_USBPLLCLK_DIV1_5;
	HAL_RCCEx_PeriphCLKConfig( & PeriphClkInit);

	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	HAL_SYSTICK_CLKSourceConfig( SYSTICK_CLKSOURCE_HCLK);

	HAL_NVIC_SetPriority(SysTick_IRQn,15,0);
}
//======================================================================================================
void vApplicationStackOverflowHook(TaskHandle_t xTask,char *pcTaskName){
	volatile int A;

	if(xTask == MainTaskHandle){
		A=BK_3.Stek_t.MainTaskStek;
		while(1){
		}
	}
	if(xTask == FlashTaskHandle){
		A=BK_3.Stek_t.FlashTaskStek;
		while(1){
		}
	}
	if(xTask == IndikTaskHandle){
		A=BK_3.Stek_t.IndTaskStek;
		while(1){
		}
	}
	if(xTask == UartTaskHandle){
		A=BK_3.Stek_t.UartTaskStek;
		while(1){
		}
	}
	if(xTask == EMULTaskHandle){
		A=BK_3.Stek_t.Emul1Stek;
		while(1){
		}
	}
	if(xTask == EMULTask1Handle){
		A=BK_3.Stek_t.Emul2Stek;
		while(1){
		}
	}
}
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
