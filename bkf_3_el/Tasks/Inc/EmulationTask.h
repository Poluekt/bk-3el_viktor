/*
 * EmulationTask.h
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */

#ifndef TASKS_INC_EMULATIONTASK_H_
#define TASKS_INC_EMULATIONTASK_H_

#define USART_DEBUG

void vTask_EMULATION_1(void const * argument);
void vTask_EMULATION(void const * argument);
void DebugSendUsartMessage(char *  str);

#endif /* TASKS_INC_EMULATIONTASK_H_ */
