/*
 * rtc.c
 *
 *  Created on: 17 нояб. 2017 г.
 *      Author: Poluect
 */
#include "rtc.h"

extern RTC_HandleTypeDef hrtc;

//===========================================================================================================
 void GetDateToUpdate(void){
	uint16_t E=HAL_RTCEx_BKUPRead( & hrtc,RTC_BKP_DR1);
	hrtc.DateToUpdate.Year=E>>8;
	hrtc.DateToUpdate.Month=E;
	E=HAL_RTCEx_BKUPRead( & hrtc,RTC_BKP_DR2);
	hrtc.DateToUpdate.Date=E>>8;
	hrtc.DateToUpdate.WeekDay=E;
}
//===========================================================================================================
void MX_RTC_Init(void){
	DATATIME_T DT;
	RTC_TimeTypeDef sTime;
	RTC_DateTypeDef DateToUpdate;

	hrtc.Instance= RTC;
	hrtc.Init.AsynchPrediv= RTC_AUTO_1_SECOND;
	hrtc.Init.OutPut= RTC_OUTPUTSOURCE_NONE;
	HAL_RTC_Init( & hrtc);
	GetDateToUpdate();

	uint16_t volatile  R=HAL_RTCEx_BKUPRead( & hrtc,RTC_BKP_DR1);
	R=0;
	if(R != 0){
		HAL_RTCEx_SetSecond_IT( & hrtc );
		return;
	}

	DT.sTime.Hours=0x19;
	DT.sTime.Minutes=0x00;
	DT.sTime.Seconds=0x0;

	DT.sDate.WeekDay= RTC_WEEKDAY_FRIDAY;
	DT.sDate.Month= RTC_MONTH_NOVEMBER;
	DT.sDate.Date=0x17;
	DT.sDate.Year=0x17;

	HAL_RTC_SetTime( & hrtc, & DT.sTime,RTC_FORMAT_BCD);
	HAL_RTC_SetDate( & hrtc, & DT.sDate,RTC_FORMAT_BCD);

	PackDateToUpdate();

	HAL_RTCEx_SetSecond_IT( & hrtc );

}
//===========================================================================================================
 void RTC_GetTime(RTC_TimeTypeDef *sTime){
	 HAL_RTC_GetTime(&hrtc, sTime, RTC_FORMAT_BIN);
	 PackDateToUpdate();
}
//===========================================================================================================
 void RTC_GetDate(RTC_DateTypeDef *sDate){
	 HAL_RTC_GetDate(&hrtc, sDate, RTC_FORMAT_BIN);
	 PackDateToUpdate();
}
//===========================================================================================================
 void RTC_GetDateTime(DATATIME_T *dt){
	 RTC_GetTime(&dt->sTime);
	 RTC_GetDate(&dt->sDate);
}
 //===========================================================================================================
  void RTC_SetDateTime(DATATIME_T *dt){
	  RTC_GetTime(&dt->sTime);
	  RTC_GetDate(&dt->sDate);
 }
//===========================================================================================================
  void PackDateToUpdate(void){
 	uint16_t E=(hrtc.DateToUpdate.Year) << 8;
 	E|=hrtc.DateToUpdate.Month;
 	HAL_RTCEx_BKUPWrite( & hrtc,RTC_BKP_DR1,E);
 	E=(hrtc.DateToUpdate.Date) << 8;
 	E|=hrtc.DateToUpdate.WeekDay;
 	HAL_RTCEx_BKUPWrite( & hrtc,RTC_BKP_DR2,E);
 }
//===========================================================================================================
