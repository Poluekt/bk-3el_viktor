/*
 * Indikacia.h
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */

#ifndef TASKS_INC_INDIKACIA_H_
#define TASKS_INC_INDIKACIA_H_

void IndikaciaTokNapr(void);
void IndikaciaTokVoltage(void);

#endif /* TASKS_INC_INDIKACIA_H_ */
