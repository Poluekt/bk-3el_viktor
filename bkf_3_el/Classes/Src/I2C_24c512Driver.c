/*
 * I2C_MemDriver.c
 *
 *  Created on: 10 нояб. 2017 г.
 *      Author: Poluect
 */
#include <I2C_MemDriver.h>
#include "stm32f1xx_hal.h"
#include "hardware_init.h"

#ifdef Mem24C512
I2C_HandleTypeDef hi2c1;

static bool WritePageToI2cRam(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress);
//====================================================================================================================
void I2C_Init(void){

	hi2c1.Instance= I2C1;
	hi2c1.Init.ClockSpeed=1000000;
	hi2c1.Init.DutyCycle= I2C_DUTYCYCLE_2;
	hi2c1.Init.OwnAddress1=0xa0;
	hi2c1.Init.AddressingMode= I2C_ADDRESSINGMODE_7BIT;
	hi2c1.Init.DualAddressMode= I2C_DUALADDRESS_DISABLE;
	hi2c1.Init.OwnAddress2=0;
	hi2c1.Init.GeneralCallMode= I2C_GENERALCALL_DISABLE;
	hi2c1.Init.NoStretchMode= I2C_NOSTRETCH_DISABLE;
	if(HAL_I2C_Init( & hi2c1) != HAL_OK){
	}
	WC_L;
}
//====================================================================================================================
HAL_StatusTypeDef ReadFromI2cRam(uint8_t* pBuffer,uint32_t length,uint32_t ReadAddress){
	uint16_t I2cDevAddress=I2C_DEVICEADDRESS;

	while(HAL_I2C_Mem_Read( & hi2c1,(uint16_t)I2C_DEVICEADDRESS,(uint16_t)ReadAddress,I2C_MEMADD_SIZE_16BIT,pBuffer,(uint16_t)length,20) != HAL_OK){
		if(HAL_I2C_GetError( & hi2c1) != HAL_I2C_ERROR_AF){
			return HAL_ERROR;
		}
	}

	return HAL_OK;
}
//=====================LED1_TOGGLE==============================================================================================
HAL_StatusTypeDef WriteToI2cRam(uint8_t* pBuffer,uint32_t length,uint32_t WriteAddress){
	uint16_t next_page , rest;
	HAL_StatusTypeDef ret=HAL_OK;
	if(WriteAddress >= VALUE_I2CMEM) return HAL_ERROR;
	if((WriteAddress + length) > VALUE_I2CMEM){
		ret=HAL_ERROR;
		length=VALUE_I2CMEM - WriteAddress;
	}
	while(length){

		next_page=(WriteAddress + I2C_PageSize) & 0xFF80;

		rest=next_page - WriteAddress;
		rest=(rest >= length) ? length : rest;
		if((WritePageToI2cRam(pBuffer,rest,WriteAddress)) != HAL_OK){
			return HAL_ERROR;
		}
		WriteAddress+=rest;
		pBuffer+=rest;
		length-=rest;
	}
	return ret;
}
//====================================================================================================================
static bool WritePageToI2cRam(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress){


  while(HAL_I2C_Mem_Write(&hi2c1,(uint16_t)I2C_DEVICEADDRESS, (uint16_t)WriteAddress, I2C_MEMADD_SIZE_16BIT, pBuffer,(uint16_t) length, 20)!= HAL_OK)
  {
    if (HAL_I2C_GetError(&hi2c1) != HAL_I2C_ERROR_AF)
    {
    	return HAL_ERROR;
    }
  }
		return HAL_OK;
}

//====================================================================================================================
#endif
