/*
 * FlashTask.h
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */

#ifndef TASKS_INC_FLASHTASK_H_
#define TASKS_INC_FLASHTASK_H_


#define ADRESS_MASSIVA_SETTINGOV 0  /* область для сеттингов   */
#define DLINA_MASSIVA_SETTINGOV  128

#define RecordsStartAddress   128   /* область для пойнтеров  */
#define KeyRecordsAddress   (RecordsStartAddress+2)
#define WriteRecordsAddress   (KeyRecordsAddress+2)
#define ReadRecordsAddress   (WriteRecordsAddress+2)

#define ADRESS_MASSIVA_DANNIX  256  /* область для рекордсов  */
#define DLINA_ZAPISI       8


#define  I2C_IMAGE_START_ADDRESS 4*1024
#define  I2C_LENGHT_IMAGE_AREA   60*1024


typedef struct {
	DATATIME_T Dt;
	uint8_t Numer_fazi;
} FLASH_MESSAGE_T;

void ERASE_FlashMemory(uint32_t start_address , uint32_t end_address);
void InitPointers(void);
HAL_StatusTypeDef StoreMomentToFlash(uint8_t N);

#endif /* TASKS_INC_FLASHTASK_H_ */
