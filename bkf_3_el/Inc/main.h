/*
 * main.h
 *
 *  Created on: 3 июля 2016 г.
 *      Author: Viktor
 */

#ifndef INC_MAIN_H_
#define INC_MAIN_H_

#include "stm32f1xx_hal.h"
#include "mb.h"
#include "INIT.h"

typedef enum Commands {
	READ_ID=0x01, SET_TIME=0x02,
	SET_SETTTINGS=0x03, GET_SETTING=0x04, GET_STATE=0x05,SET_SWITCH_FAZE=0x06,GET_POOL_DATA=0x07,CONFIRM_DATA=0x08,
	START_DOWNLOADER=0x41, STOP_DOWNLOADER=0x42, WRITE_IMAGE_BLOCK=0x43, IMAGE_CRC=0x44, SOFTWARE_RESET=0x50,
	SAVE_BOOTLOADER=0x51,
}Commands;
typedef struct {
	uint32_t MainTaskStek;
	uint32_t IndTaskStek;
	uint32_t FlashTaskStek;
	uint32_t UartTaskStek;
	uint32_t Heap;
	uint32_t TestStek;
	uint32_t Emul1Stek;
	uint32_t Emul2Stek;
	uint32_t Emul3Stek;
} STEK_T;
typedef enum {
	CONNECT=1, DISCONNECT=0
} ConnectState_E;

typedef enum {
	SVARIVANIE_KONTAKTA=1, OBRIV_KONTAKTA=2, NO_ERR=0
} Error_Rele;

typedef enum {
	NADO_PEREKLUCHAT6=1, WAITING_OTKLUCHENIA=2, WAITING_VKLUCHENIA=256, NORMAL_WORK=4, SVARIVANIE_RELE=128,
	OBRIV_RELE=8, NACHALNOE_VKLUCHENIE=16, AVOST=32, STOP_JOB=64,
} BK_3L_State;
typedef enum {
	Otkluchit6=1, Ostavit6_kak_est6=0
} Politica;

typedef struct {
	float F1_EffectValue;
	float F2_EffectValue;
	float F3_EffectValue;
	float OS_EffectValue;
	float Tok_EffectValue;
} Test_T;

typedef struct {
	float Level_of_presence;             // уровень присутствия
	float Max_value;
	float Min_value;
	float Koeff_mashtabirovania;         // из аппаратных в действующее значение
} Setting_Faz_T;

typedef struct {
	float Effecient_value;               // эффективное значение
	float Indikat_value;                 //ИНДИЦИРУЕМОЕ ЗНАЧЕНИЕ
	float Fase;                          // фаза в градусах относ F1
	ConnectState_E ConnectStatus;          // замкнуто реле  или нет
	Error_Rele Error_REle;
} Status_Faz_T;

typedef struct {
	uint8_t Numer;	                    // номер по порядку 1-5  F1 F2 F3 F_OS F_TOK
	uint8_t Flag_of_presence;            // флаг присутствия напряжения фазы
	uint8_t Count_of_unpresense;           // счетчик периодов отсутствия
	uint8_t Count_of_presense;           // счетчик периодов присутствия
	uint8_t Flag_of_out_up_max_value;    // флаг превышения
	uint8_t Flag_of_out_down_min_value;    // флаг понижения ниже мин уровня
	uint8_t Count_of_min_value_flag;     // счетчик периодов понижения
	uint8_t Count_of_max_value_flag;     // счетчик периодов превышения
	uint16_t Count_of_periodov_of_normal_work;    // счетчик периодов нормальной работы

	Status_Faz_T StatusFaz;
	Setting_Faz_T SettingFaz;
} FAZA_t;

typedef struct {
	uint16_t FlagZapisiSettingov; /*!< Равен коду если сеттинги уже находятся во флэше */
	uint8_t NumerHighPriorityFase;      // НОМЕР  ФАЗЫ C MAX ПРИОРИТЕТОМ
	uint8_t NumerMediumPriorityFase;    // НОМЕР  ФАЗЫ CО СРЕДНИМ ПРИОРИТЕТОМ
	uint8_t NumerMLowPriorityFase;       // НОМЕР  ФАЗЫ C НИЗШИМ ПРИОРИТЕТОМ
	Politica Politika_nevozmognosti_perekluchenia; /*!< труктура используется для отладки при эмуляции */
} SETTING_T;
typedef struct {
	uint8_t NumerFaseForSwitch;       // НОМЕР ПОДКЛЮЧЕННОЙ ФАЗЫ
	uint16_t WriteAddress;
	uint16_t ReadAddress;
	BK_3L_State State;
} BK_3_T;
typedef struct{
//uint32_t N;
BK_3L_State TrEvent;
}Trace_T;
typedef struct {
	char id[20];
	FAZA_t *pF[5];
	uint16_t Counter40TFromCommut;     // СЧЕТЧИК 40 ПЕРИОДОВ С МОМЕНТА КОММУТАЦИИ
	uint16_t CounterTFromSvariv;    // СЧЕТЧИК ПЕРИОДОВ С МОМЕНТА  SVARIVANIE_RELE
	uint16_t CounterTFromObriv;    // СЧЕТЧИК ПЕРИОДОВ С МОМЕНТА OBRIV RELE
	uint32_t CounterTFromAvost;    // СЧЕТЧИК ПЕРИОДОВ С МОМЕНТА AVOST
	uint32_t Counter10mTFromAvost; // СЧЕТЧИК ПЕРИОДОВ С МОМЕНТА AVOST - ИСПОЛЗУЕТСЯ ДЛЯ ОТСЧЕТА 10 МИН ДЛЯ ОБНУЛЕНИЯ К-В АВОСТА
	uint8_t NumerWorkingFase;               // НОМЕР ПОДКЛЮЧЕННОЙ ФАЗЫ
	uint8_t NumerFaseForPushUp_1;    // НОМЕР  ФАЗЫ КАНДИДАТА НА ВКЛЮЧЕНИЕ 1
	uint8_t NumerFaseForPushUp_2;    // НОМЕР  ФАЗЫ КАНДИДАТА НА ВКЛЮЧЕНИЕ 2
	uint8_t Nomer1;    // РЕАЛЬНЫЙ КАНДИДАТ НА ПЕРЕКЛЮЧЕНИЕ N 1
	uint8_t Nomer2;    // РЕАЛЬНЫЙ КАНДИДАТ НА ПЕРЕКЛЮЧЕНИЕ N 2
	FIRMWARE_T firmware;
	STEK_T Stek_t;
	Test_T test; /*!< труктура используется для отладки при эмуляции */
	FAZA_t F1;
	FAZA_t F2;
	FAZA_t F3;
	FAZA_t F_OS;
	FAZA_t F_TOK;
	BK_3_T BK3status;
	SETTING_T Setting;
} BK_3L;

void vTaskMain(void *pvParametrs);
void vTask_FLASH(void  * argument);
void vTask_UART(void const * argument);
void StoreStatusToPointer(UCHAR * pucRegBuffer,UCHAR * J);
void UnpackPointerToSettings(uint8_t *MASSIv,uint8_t * J);
void UsbHandler(uint8_t* buf,uint32_t *length);
#endif /* INC_MAIN_H_ */
