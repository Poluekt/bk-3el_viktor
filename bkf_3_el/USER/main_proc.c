/*
 * main_proc.c
 *
 *  Created on: 1 июля 2016 г.
 *      Author: Viktor
 */

#include "BK_3el.h"

extern ADC_HandleTypeDef hadc1;
extern BK_3L BK_3;

extern xQueueHandle Ququ_indik;    // messages to indikacia
extern xQueueHandle Ququ_Uart;
//extern LCD_STATUS_T StatusLcd;
extern unsigned char FLAG_CMD_SWITCH;
extern unsigned int volatile CO;
uint32_t schetchik_avosta=0;
uint8_t schetchik_k_v_avosta=0;
xQueueHandle Ququ_flash;

Trace_T TR[100];
uint8_t K=0;

//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура выполняет все операции с подсчитыванием количеств Avost.
 * @retval None
 */
void AvostHandler(void){
	// Если это первый Avost - запускаем счетчик отсчета 10 мин с момента Avost
	if(schetchik_k_v_avosta == 0){
		BK_3.Counter10mTFromAvost=1;
		DebugSendUsartMessage("AVOST1 \r\n");  // сообщение о первом Avost
	}
	// Если это второй Avost - запускаем счетчик отсчета 10 мин с момента Avost
	if(schetchik_k_v_avosta == 1){
		BK_3.Counter10mTFromAvost=1;
		DebugSendUsartMessage("AVOST2 \r\n");  // сообщение о втором Avost
	}

	BK_3.BK3status.State=NADO_PEREKLUCHAT6;
	// Если это третий Avost - запускаем счетчик отсчета 10 мин с момента Avost
	if(schetchik_k_v_avosta == 2){
		BK_3.Counter10mTFromAvost=1;
		DebugSendUsartMessage("AVOST3  \r\n");
		; // сообщение о третьем Avost
		BK_3.BK3status.State=STOP_JOB;  // Если это третий Avost  то останавливаемся
	}

	schetchik_k_v_avosta++;

	BK_3.CounterTFromAvost=0;

	__HAL_ADC_ENABLE_IT( & hadc1,ADC_IT_AWD);
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief
 */
__weak void DebugSendUsartMessage(char * str){
	UNUSED(str);
}
//-------------------------------------------------------------------------------------------------------
void FindKandidats(void){
	uint8_t Nomer4 , Nomer42 , Nomer43;

	Nomer42=Nomer43=Nomer4=0;

// Берем фазы начиная с высшего приоритета .Если она в порядке - то она первая кандидатка.
	uint8_t i;
	i=BK_3.Setting.NumerHighPriorityFase;
	if((BK_3.pF[i - 1]->StatusFaz.ConnectStatus != CONNECT)
			&& (BK_3.pF[i - 1]->Numer != BK_3.NumerWorkingFase)){
		if(BK_3.pF[i - 1]->StatusFaz.Error_REle == NO_ERR){
			if( ! (BK_3.pF[i - 1]->Flag_of_out_up_max_value || BK_3.pF[i - 1]->Flag_of_out_down_min_value)){
				if(BK_3.pF[i - 1]->StatusFaz.Effecient_value > BK_3.pF[i - 1]->SettingFaz.Level_of_presence){
					Nomer4=BK_3.pF[i - 1]->Numer;
				}
			}
		}
	}

	i=BK_3.Setting.NumerMediumPriorityFase;
	if((BK_3.pF[i - 1]->StatusFaz.ConnectStatus != CONNECT)
			&& (BK_3.pF[i - 1]->Numer != BK_3.NumerWorkingFase)){
		if(BK_3.pF[i - 1]->StatusFaz.Error_REle == NO_ERR){
			if( ! (BK_3.pF[i - 1]->Flag_of_out_up_max_value || BK_3.pF[i - 1]->Flag_of_out_down_min_value)){
				if(BK_3.pF[i - 1]->StatusFaz.Effecient_value > BK_3.pF[i - 1]->SettingFaz.Level_of_presence){
					Nomer42=BK_3.pF[i - 1]->Numer;
				}
			}
		}
	}
	i=BK_3.Setting.NumerMLowPriorityFase;
	if((BK_3.pF[i - 1]->StatusFaz.ConnectStatus != CONNECT)
			&& (BK_3.pF[i - 1]->Numer != BK_3.NumerWorkingFase)){
		if(BK_3.pF[i - 1]->StatusFaz.Error_REle == NO_ERR){
			if( ! (BK_3.pF[i - 1]->Flag_of_out_up_max_value || BK_3.pF[i - 1]->Flag_of_out_down_min_value)){
				if(BK_3.pF[i - 1]->StatusFaz.Effecient_value > BK_3.pF[i - 1]->SettingFaz.Level_of_presence){
					Nomer43=BK_3.pF[i - 1]->Numer;
				}
			}
		}
	}

	if(Nomer4 == 0){
		Nomer4=Nomer42;
		Nomer42=Nomer43;
	}
	if(Nomer4 == 0){
		Nomer4=Nomer42;
		Nomer42=0;
	}
	if(Nomer42 == 0){
		Nomer42=Nomer43;
	}

	BK_3.NumerFaseForPushUp_1=Nomer4;
	BK_3.NumerFaseForPushUp_2=Nomer42;

}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура ищет кандидатов на включение - кроме рабочей фазы - при переключении и отключает если нашла.
 * @retval None
 */
uint8_t Co0=0 , Co1=0 , Co2=0 , Co3=0;
void FindFaseForPushUp(void){
	BK_3.Nomer1=0;
	BK_3.Nomer2=0;
//	uint8_t CO0 , CO1 , CO2 , CO3;
//-----ПРОЦЕДУРА ИЗ 2-Х КАНДИДАТОВ НА ВКЛЮЧЕНИЕ ВЫБИРАЕТ ФАЗЫ ДЛЯ ВКЛЮЧЕНИЯ NOMER1 NOMER2 -------------------------------------------------
//-----СНАЧАЛА ОНА ПЫТАЕТСЯ НАЙТИ ФАЗЫ КОТОРЫЕ ДЛИТЕЛЬНО НОРМАЛЬНО РАБОТАЮТ А ПОТОМ ЕСЛИ ИХ НЕТ ТО ПРОСТО ИЗ ЧИСЛА КАНДИДАТОВ------------
//-----ЕСЛИ ФАЗА НОРМАЛЬНО ДЛИТЕЛЬНО РАБОТАЕТ -- 1 КАНДИДАТ НА ВКЛЮЧЕНИЕ  ------------------

	FindKandidats();  // имеем NUMER_FASE_KANDIDATA_NA_VKLUCHENIE_1,2 нормально работающие
	/*	 ИЗ 2-Х КАНДИДАТОВ НА ВКЛЮЧЕНИЕ ИЩЕМ СТАБИЛЬНО РАБОТАЮЩИЕ*/
	if(BK_3.NumerFaseForPushUp_1) if(BK_3.pF[BK_3.NumerFaseForPushUp_1 - 1]->Count_of_periodov_of_normal_work
			> 15000) BK_3.Nomer1=BK_3.NumerFaseForPushUp_1;

//	 ЕСЛИ 1 КАНДИДАТ НА ВКЛЮЧЕНИЕ НЕ  НОРМАЛЬНО ДЛИТЕЛЬНО РАБОТАЕТ- ПРОБУЕМ 2 КАНДИДАТА НА ВКЛЮЧЕНИЕ-
	if(BK_3.NumerFaseForPushUp_2) if(BK_3.pF[BK_3.NumerFaseForPushUp_2 - 1]->Count_of_periodov_of_normal_work
			> 15000) BK_3.Nomer2=BK_3.NumerFaseForPushUp_2;

	/*---------ЕСЛИ НЕТ НОРМАЛЬНО ДЛИТЕЛЬНО РАБОТАЮЩИХ ФАЗ ИЗ 2-Х КАНДИДАТОВ НА ВКЛЮЧЕНИ------------------
	 -----------ТОГДА БЕРЕМ ФАЗУ ЕСЛИ ОНА ПРОСТО НОРМАЛЬНАЯ ИЗ КАНДИДАТОВ НА ВКЛЮЧЕНИЕ -------------*/
	if(BK_3.Nomer1 == 0){
		BK_3.Nomer1=BK_3.Nomer2;
		BK_3.Nomer2=0;
	}
	if((BK_3.Nomer1 == 0) && (BK_3.Nomer2 == 0) && (BK_3.NumerFaseForPushUp_1 != 0)) BK_3.Nomer1=BK_3
			.NumerFaseForPushUp_1;
	/* посылаем отладочное сообщение */
	if(BK_3.Nomer1 == 0){
		Co1=Co2=Co3=0;
		if(Co0 == 0){
			DebugSendUsartMessage("FIND 0\r\n");
			Co0++;
		}else{
			Co0++;
		}
		/* если нет фазы куда переключаться то объявляем отключившуюся фазу DISCONNECT чтобы к ней подключиться */
		if(BK_3.Nomer1 == 0){
			if(BK_3.F_OS.Count_of_unpresense){
				// OS ОТКЛЮЧИЛАСЬ
				BK_3.pF[BK_3.NumerWorkingFase - 1]->StatusFaz.ConnectStatus=DISCONNECT;
				BK_3.NumerWorkingFase=0;
			}
		}
	}
	/* если есть команда на переключение от хоста то смотрим можно ли переключить на фазу хоста BK_3.Nomer1 */
	if(BK_3.BK3status.NumerFaseForSwitch != 0){
		uint8_t F=BK_3.BK3status.NumerFaseForSwitch;
		if(F == BK_3.Nomer1) BK_3.Nomer1=F;
		else if(F == BK_3.Nomer1) BK_3.Nomer1=F;
		else if(F == BK_3.NumerFaseForPushUp_1) BK_3.Nomer1=F;
		else if(F == BK_3.NumerFaseForPushUp_2) BK_3.Nomer1=F;

		if(BK_3.Nomer1 != 0) BK_3.BK3status.NumerFaseForSwitch=0;

	}
	/* посылаем отладочное сообщение */
	if(BK_3.Nomer1 == 1){
		Co0=Co2=Co3=0;
		if(Co1 == 0){
			DebugSendUsartMessage("FIND 1\r\n");
			Co1++;
		}else{
			Co1++;
		}
	}
	if(BK_3.Nomer1 == 2){
		Co0=Co1=Co3=0;
		if(Co2 == 0){
			DebugSendUsartMessage("FIND 2\r\n");
			Co2++;
		}else{
			Co2++;
		}
	}
	if(BK_3.Nomer1 == 3){
		Co0=Co2=Co1=0;
		if(Co3 == 0){
			DebugSendUsartMessage("FIND 3\r\n");
			Co3++;
		}
	}else{
		Co3++;
	}

	if(Co0 >= 175) Co0=0;
	if(Co1 >= 175) Co1=0;
	if(Co2 >= 175) Co2=0;
	if(Co3 >= 175) Co3=0;
}
//-------------------------------------------------------------------------------------------------------
void Operacia(void){

	if(BK_3.BK3status.NumerFaseForSwitch != 0){
		BK_3.BK3status.State=NADO_PEREKLUCHAT6;
	}

	switch(BK_3.BK3status.State){

		case NACHALNOE_VKLUCHENIE:
			ProcNachalnogoVkluchenia();
		break;

		case NORMAL_WORK:
			ProcNormalWork();
		break;

		case NADO_PEREKLUCHAT6:
			FindFaseForPushUp();
			OtkluchenieHandler();
		break;

		case WAITING_OTKLUCHENIA:
			ProcWaitingOtkluchenia();
		break;

		case WAITING_VKLUCHENIA:
			ProcWaitingVkluchenia();
		break;

		case SVARIVANIE_RELE:
			SvarivanieHandler();
		break;

		case OBRIV_RELE:
			ObrivReleHandler();
		break;

		case AVOST:
			AvostHandler();
		break;

		case STOP_JOB:
			StopJobHandler();
		break;

	}
	ObrivHandler();
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура выполняет все операции с подсчитыванием периодов после  обрыва реле.
 *         Через 20 мин после обрыва помечаем реле как нормальное
 * @retval None
 */
void ObrivReleHandler(void){
	BK_3.BK3status.State=NADO_PEREKLUCHAT6;
	DebugSendUsartMessage("OBRIV NADO PEKLUCHAT6\r\n");
}
//-------------------------------------------------------------------------------------------------------
void ObrivHandler(void){
	// ЧЕРЕЗ 20 МИН ПОМЕТИМ РЕЛЕ КАК НОРМАЛЬНЫЕ
	if(BK_3.CounterTFromObriv > (1 * 50 * 30)){
		if(BK_3.F1.StatusFaz.Error_REle == OBRIV_KONTAKTA) BK_3.F1.StatusFaz.Error_REle=NO_ERR;
		if(BK_3.F2.StatusFaz.Error_REle == OBRIV_KONTAKTA) BK_3.F2.StatusFaz.Error_REle=NO_ERR;
		if(BK_3.F3.StatusFaz.Error_REle == OBRIV_KONTAKTA) BK_3.F3.StatusFaz.Error_REle=NO_ERR;

		DebugSendUsartMessage("OBRIV RELEASE\r\n");
		BK_3.CounterTFromObriv=0;

	}else{
		if(BK_3.CounterTFromObriv) BK_3.CounterTFromObriv++;
	}
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура выполняет отключение реле.
 */
void Otkluchit6_all(void){
	RELAY_1_OFF;
	RELAY_2_OFF;
	RELAY_3_OFF;
}
//-------------------------------------------------------------------------------------------------------}
/**
 * @brief  Процедура выполняет все операции с подсчитыванием периодов после  обрыва реле.
 *         Через 20 мин после обрыва помечаем реле как нормальное
 * @retval None
 */
void OtkluchenieHandler(void){
	/* Включаем фазу если есть какую если нет - то политике невозможности переключения */
	if(BK_3.Nomer1 == 0){
		if(BK_3.Setting.Politika_nevozmognosti_perekluchenia == Otkluchit6){
			Otkluchit6_all();
			BK_3.BK3status.State=NADO_PEREKLUCHAT6;
			DebugSendUsartMessage("TURN OFF BY POLITICS\r\n");
			BK_3.Counter40TFromCommut=0;
		}else{
//			BK_3.STATus=NORMAL_WORK;

//			BK_3.F1.Flag_of_out_up_max_value=0;
//			BK_3.F2.Flag_of_out_up_max_value=0;
//			BK_3.F3.Flag_of_out_up_max_value=0;
//
//			BK_3.F1.Count_of_min_value_flag=0;
//			BK_3.F2.Count_of_min_value_flag=0;
//			BK_3.F3.Count_of_min_value_flag=0;
//			BK_3.Counter40TFromCommut=0;
		}
	}else{
		Otkluchit6_all();
		BK_3.BK3status.State=WAITING_OTKLUCHENIA;
		BK_3.Counter40TFromCommut=0;
		DebugSendUsartMessage("TURN OFF\r\n");
	}
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура проверяет надо ли переключать рабочую фазу .
 * @retval Переключает статус прибора
 */
void ProcNormalWork(void){
	static uint8_t H , H1;
	/*	сообщаем о нормальной работе */
	if(H1 == 0){
		DebugSendUsartMessage("NORMAL WORK\r\n");
		H1++;
	}else{
		H1++;
		if(H1 >= 150){
			H1=0;
		}
	}
	/*проверка  фазы ОС  */
	if(BK_3.F_OS.Flag_of_out_up_max_value || BK_3.F_OS.Flag_of_out_down_min_value){
		BK_3.BK3status.State=NADO_PEREKLUCHAT6;
	}

	/*	сообщаем о результатах */
	if(BK_3.BK3status.State == NADO_PEREKLUCHAT6){
		DebugSendUsartMessage("NADO_PEREKLUCHAT6\r\n");
		H=0;
	}
	if((BK_3.BK3status.State != NADO_PEREKLUCHAT6) && (H == 0)){
//		DebugSendUsartMessage("NE NADO_PEREKLUCHAT6\r\n");
	}
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура выполняет поиск кандидата для начального включения .
 *         Определяет NUMER1 IZ 3 FAS ПО ПРИОРИТЕТАМ НАХОДЯТСЯ ЛИ ОНИ В ПРЕДЕЛАХ границ И ПОРЯДКЕ ИЛИ НЕТ.
 * @retval None
 */
void ProcNachalnogoVkluchenia(void){
	BK_3.Nomer1=0;
	BK_3.Nomer2=0;

	Otkluchit6_all();
	BK_3.BK3status.State=NADO_PEREKLUCHAT6;
	BK_3.NumerWorkingFase=0;
}
/**
 * @brief  Процедура ищет кандидатов на включение - кроме рабочей фазы - при переключении .
 * @retval None
 */
//-------------------------------------------------------------------------------------------------------
void ProcSetAvost(void){
	static CMD_INDIKACII_T A;
	BaseType_t xHigherPriorityTaskWoken= pdFALSE;
	CMD_UART_T B;

	BK_3.BK3status.State=AVOST;
	Otkluchit6_all();
	BK_3.CounterTFromAvost=0;
	__HAL_ADC_DISABLE_IT( & hadc1,ADC_IT_AWD);

	A=Sverhtok;
	xQueueSendFromISR(Ququ_indik, & A, & xHigherPriorityTaskWoken);

	B=SVERHTOK1;
	xQueueSendFromISR(Ququ_Uart, & B, & xHigherPriorityTaskWoken);

}
//-------------------------------------------------------------------------------------------------------
void ProcSetAvost1(void){
// ДЛЯ ЭМУЛЯЦИИ АВОСТА ИЗ ЗАДАЧИ
	static CMD_INDIKACII_T A;
	CMD_UART_T B;

	BK_3.BK3status.State=AVOST;
	Otkluchit6_all();
	BK_3.CounterTFromAvost=0;
	__HAL_ADC_DISABLE_IT( & hadc1,ADC_IT_AWD);

	A=Sverhtok;
	xQueueSendToBack(Ququ_indik, & A,0);

	/*B=SVERHTOK1 ;
	 xQueueSendToBack( Ququ_Uart , & B , 0 );*/
}
//-------------------------------------------------------------------------------------------------------
void ProcWaitingOtkluchenia(void){
// ЖДЕМ 40 ПЕРИОДОВ И ПРОВЕРЯЕМ ЕСТЬ ЛИ НА ОС НАПРУГА И ТОК
// ЕСЛИ НЕТУ ТО ПРОИЗВОДИМ ВКЛЮЧЕНИЕ И СТАТУС= WAITING_VKLUCHENIA;
// ЕСЛИ НАПРУГА ИЛИ ТОК ОСТАЛИСЬ  TO СТАВИМ STATUS SVARIVANIE_RELE И  STATUS PELE SVARIVANIE_KONTAKTA
//	ЕСЛИ СИТУАЦИЯ ПОВТОРИЛАСЬ 5 РАЗ
//----------------------------------------------------------------------------
	if(BK_3.Counter40TFromCommut >= 20){

// ------------НОРМАЛЬНО - ПЕРЕКЛЮЧАЕМСЯ --------------------------------------

		if((BK_3.F_OS.StatusFaz.Effecient_value < BK_3.F_OS.SettingFaz.Level_of_presence)
				&& (BK_3.F_TOK.StatusFaz.Effecient_value < BK_3.F_TOK.SettingFaz.Level_of_presence)){
			BK_3.F1.StatusFaz.ConnectStatus=DISCONNECT;
			BK_3.F2.StatusFaz.ConnectStatus=DISCONNECT;
			BK_3.F3.StatusFaz.ConnectStatus=DISCONNECT;
			Vkluchit6();
			BK_3.F1.StatusFaz.ConnectStatus=BK_3.F2.StatusFaz.ConnectStatus=BK_3.F2.StatusFaz.ConnectStatus=
					DISCONNECT;
			DebugSendUsartMessage("WAIT VKLUCHENIA\r\n");
			BK_3.BK3status.State=WAITING_VKLUCHENIA;
			BK_3.Counter40TFromCommut=0;
		}else{
			if(BK_3.Counter40TFromCommut >= 40){

//--------------НЕНОРМАЛЬНО НЕ ПЕРЕКЛЮЧАЕМСЯ ---------------------------------------

				BK_3.BK3status.State=SVARIVANIE_RELE;
				BK_3.CounterTFromSvariv=0;

				switch(BK_3.NumerWorkingFase){
					case 1:
						BK_3.F1.StatusFaz.Error_REle=SVARIVANIE_KONTAKTA;
						DebugSendUsartMessage("SVARIVANIE 1\r\n");
						SendMessageToIndikacia(ZALIP1);
					break;
					case 2:
						BK_3.F2.StatusFaz.Error_REle=SVARIVANIE_KONTAKTA;
						DebugSendUsartMessage("SVARIVANIE 2\r\n");
						SendMessageToIndikacia(ZALIP2);
					break;
					case 3:
						BK_3.F3.StatusFaz.Error_REle=SVARIVANIE_KONTAKTA;
						DebugSendUsartMessage("SVARIVANIE 3\r\n");
						SendMessageToIndikacia(ZALIP3);
					break;
					case 0:   // ФАЗЫ ЕЩЕ ВООБЩЕ НЕ ВКЛЮЧАЛИСЬ НАХОДИМ ФАЗУ СО СВАРЕННЫМИ КОНТАКТАМИ
						if((BK_3.F_OS.StatusFaz.Fase < (BK_3.F1.StatusFaz.Fase + 0.5))
								&& (BK_3.F_OS.StatusFaz.Fase > (BK_3.F1.StatusFaz.Fase - 0.5))){
							BK_3.F1.StatusFaz.Error_REle=SVARIVANIE_KONTAKTA;
							DebugSendUsartMessage("SVARIVANIE  0 1\r\n");
							SendMessageToIndikacia(ZALIP1);
							break;
						}
						if((BK_3.F_OS.StatusFaz.Fase < (BK_3.F2.StatusFaz.Fase + 0.5))
								&& (BK_3.F_OS.StatusFaz.Fase > (BK_3.F2.StatusFaz.Fase - 0.5))){
							BK_3.F2.StatusFaz.Error_REle=SVARIVANIE_KONTAKTA;
							DebugSendUsartMessage("SVARIVANIE  0 2\r\n");
							SendMessageToIndikacia(ZALIP2);
							break;
						}
						if((BK_3.F_OS.StatusFaz.Fase < (BK_3.F3.StatusFaz.Fase + 0.5))
								&& (BK_3.F_OS.StatusFaz.Fase > (BK_3.F3.StatusFaz.Fase - 0.5))){
							BK_3.F3.StatusFaz.Error_REle=SVARIVANIE_KONTAKTA;
							DebugSendUsartMessage("SVARIVANIE  0 3\r\n");
							SendMessageToIndikacia(ZALIP3);
							break;
						}
					break;
				}
			}else BK_3.Counter40TFromCommut++;
		}
	}else{
		BK_3.Counter40TFromCommut++;
	}
}
//-------------------------------------------------------------------------------------------------------
void ProcWaitingVkluchenia(void){
// ESLI CHERES 40 PERIODOV NET NAPRUGI TO OBRIV_RELE I OBRIV_KONTAKTA
// ESLI EST - TO

	/* Пауза включения прошла ?*/
	if(BK_3.Counter40TFromCommut >= 20){
		/* ОС в норме  ? */
		if((BK_3.Counter40TFromCommut >= 40)
				&& ((BK_3.F_OS.StatusFaz.Effecient_value < BK_3.F_OS.SettingFaz.Level_of_presence))){
			/* НЕТ - обрыв реле  ? */
			BK_3.BK3status.State=OBRIV_RELE;
			BK_3.CounterTFromObriv=1;
			switch(BK_3.Nomer1){
				case 1:
					BK_3.F1.StatusFaz.Error_REle=OBRIV_KONTAKTA;
					DebugSendUsartMessage("OBRIV 1\r\n");
					SendMessageToIndikacia(OBR_R_1);
				break;
				case 2:
					BK_3.F2.StatusFaz.Error_REle=OBRIV_KONTAKTA;
					DebugSendUsartMessage("OBRIV 2\r\n");
					SendMessageToIndikacia(OBR_R_2);
				break;
				case 3:
					BK_3.F3.StatusFaz.Error_REle=OBRIV_KONTAKTA;
					DebugSendUsartMessage("OBRIV 3\r\n");
					SendMessageToIndikacia(OBR_R_3);
				break;
			}

		}
		/* ДА  - Включение произошло  ? */
		if(BK_3.F_OS.StatusFaz.Effecient_value > BK_3.F_OS.SettingFaz.Level_of_presence){

			BK_3.BK3status.State=NORMAL_WORK;
			BK_3.NumerWorkingFase=BK_3.Nomer1;
			switch(BK_3.Nomer1){
				case 1:
					DebugSendUsartMessage("VKLUCHENIE 1\r\n");
					BK_3.F1.StatusFaz.ConnectStatus=CONNECT;
					BK_3.F1.StatusFaz.Error_REle=NO_ERR;
					SendMessageToFlash(store_moment_1);
				break;
				case 2:
					DebugSendUsartMessage("VKLUCHENIE 2\r\n");
					BK_3.F2.StatusFaz.ConnectStatus=CONNECT;
					BK_3.F2.StatusFaz.Error_REle=NO_ERR;
					SendMessageToFlash(store_moment_2);
				break;
				case 3:
					DebugSendUsartMessage("VKLUCHENIE 3\r\n");
					BK_3.F3.StatusFaz.ConnectStatus=CONNECT;
					BK_3.F3.StatusFaz.Error_REle=NO_ERR;
					SendMessageToFlash(store_moment_3);
				break;
				default:
					DebugSendUsartMessage("VKLUCHENIE 0\r\n");
				break;
			}

		}else if(BK_3.Counter40TFromCommut < 41) BK_3.Counter40TFromCommut++;
	}else{
		if(BK_3.Counter40TFromCommut < 41) BK_3.Counter40TFromCommut++;
	}
}
//--------------------------------------------------------------------------------------
void Stalking(FAZA_t * F){
// ОПРЕДЕЛЯЕТ НАЛИЧИЕ ПРИСУТСТВИЯ ФАЗЫ ,ФЛАГИ ПРЕВЫШЕНИЯ И ПРЕНИЖЕНИЯ ФАЗЫ , ВРЕМЯ НОРМАЛЬНОЙ РАБОТЫ
	if(F->StatusFaz.Effecient_value > F->SettingFaz.Level_of_presence){
		F->Count_of_unpresense=0;
		if(F->Count_of_presense < 40) F->Count_of_presense++;
	}else{
		F->Count_of_presense=0;
		if(F->Count_of_unpresense < 40) F->Count_of_unpresense++;
	}

	if(F->Count_of_presense > 39) F->Flag_of_presence=1;
	if(F->Count_of_unpresense > 39) F->Flag_of_presence=0;

//--------------------------------------------------------------------------------------
	if(F->StatusFaz.Effecient_value > F->SettingFaz.Max_value){
		if(F->Count_of_max_value_flag < 40) F->Count_of_max_value_flag++;
	}else{
		F->Count_of_max_value_flag=0;
	}

	if(F->Count_of_max_value_flag > 39){
		F->Flag_of_out_up_max_value=1;
	}else{
		F->Flag_of_out_up_max_value=0;
	}
//--------------------------------------------------------------------------------------
	if(F->StatusFaz.Effecient_value < F->SettingFaz.Min_value){
		if(F->Count_of_min_value_flag < 40) F->Count_of_min_value_flag++;
	}else{
		F->Count_of_min_value_flag=0;
	}

	if(F->Count_of_min_value_flag > 39){
		F->Flag_of_out_down_min_value=1;
	}else{
		F->Flag_of_out_down_min_value=0;
	}
//--------------------------------------------------------------------------------------
	if((F->Count_of_min_value_flag == 0) && (F->Count_of_max_value_flag == 0)){
		if(F->Count_of_periodov_of_normal_work < 30000) F->Count_of_periodov_of_normal_work++;
	}else{
		F->Count_of_periodov_of_normal_work=0;
	}
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура выполняет выход на нрмальную работу после  Avostов.
 * ерез 10 мин после серии Авостов снова идем на включение
 * @retval None
 */
void StopJobHandler(void){
	BK_3.Counter10mTFromAvost++;
	if(BK_3.Counter10mTFromAvost >= (10 * 50 * 60)){
		BK_3.Counter10mTFromAvost=0;
		schetchik_k_v_avosta=0;
		BK_3.BK3status.State=NACHALNOE_VKLUCHENIE;
	}
}
//-------------------------------------------------------------------------------------------------------
/**
 * @brief  Процедура выполняет все операции с подсчитыванием периодов сваривания реле.
 *         Через 20 сек снова проверяем на сваривание .
 * @retval None
 */
void SvarivanieHandler(void){
	if(BK_3.CounterTFromSvariv > (20 * 50)){
		BK_3.F1.StatusFaz.Error_REle=NO_ERR;
		BK_3.F2.StatusFaz.Error_REle=NO_ERR;
		BK_3.F3.StatusFaz.Error_REle=NO_ERR;
		BK_3.BK3status.State=WAITING_OTKLUCHENIA;
		BK_3.Counter40TFromCommut=0;
	}else{
		BK_3.CounterTFromSvariv++;
	}
}
//-------------------------------------------------------------------------------------------------------
void TestNormalWork(void){

	if(BK_3.pF[4]->Flag_of_out_up_max_value || BK_3.pF[4]->Flag_of_out_down_min_value){
		BK_3.BK3status.State=NADO_PEREKLUCHAT6;
	}
}
//-------------------------------------------------------------------------------------------------------
void Vkluchit6(void){
	switch(BK_3.Nomer1){
		case 1:
			RELAY_1_ON;
		break;
		case 2:
			RELAY_2_ON;
		break;
		case 3:
			RELAY_3_ON;
		break;
		default:
			;
	}

	SendMessageToIndikacia(Perekluchenie);
}
//-------------------------------------------------------------------------------------------------------
