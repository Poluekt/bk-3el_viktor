/*
 * TIMES.c
 *
 *  Created on: 1 февр. 2016 г.
 *      Author: Viktor
 */
#include "stm32f1xx_hal.h"
//#include "INIT.h"

extern __IO uint32_t global_count_seconds ;
//===================================================================================================================================
void HAL_RTCEx_RTCEventCallback ( RTC_HandleTypeDef *hrtc )
{
// IRQ  КАЖДУЮ СЕКУНДУ ОТ RTC

	global_count_seconds ++;

}
//===================================================================================================================================

