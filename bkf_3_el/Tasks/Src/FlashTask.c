/*
 * FlashTask.c
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */
#include "BK_3el.h"
extern RTC_HandleTypeDef hrtc;
extern xQueueHandle Ququ_flash;
extern BK_3L BK_3;
extern FIRMWARE_T firmware;
extern  xSemaphoreHandle xI2CSemaphore;
static void UPDATE_Bootloader(void);
/**
 * @brief  Процедура стирает страницы флэша контроллера.
 */
void ERASE_FlashMemory(uint32_t start_address,uint32_t end_address){
	FLASH_EraseInitTypeDef EraseInitStruct;

	uint32_t PAGEError=0;

	EraseInitStruct.TypeErase= FLASH_TYPEERASE_PAGES;
	EraseInitStruct.PageAddress=start_address;       //FLASH_IMAGE_START_ADDR;
	EraseInitStruct.NbPages=(end_address - start_address)/*(FLASH_IMAGE_END_ADDR - FLASH_IMAGE_START_ADDR)*/
	/ FLASH_PAGE_SIZE;

	/* очищаем память под образ программы */
	HAL_FLASH_Unlock();
	if(HAL_FLASHEx_Erase( & EraseInitStruct, & PAGEError) != HAL_OK){
	}
	HAL_FLASH_Lock();
}
//========================================================================================================
/**
 * @brief  Задача производит операции с и2с флэшем и флэшем контроллера.
 */

//====================================================================================================================
void InitPointers(void){
	uint16_t Key;
	HAL_StatusTypeDef volatile ret , ret1=HAL_OK;

	ret=ReadFromI2cRam((uint8_t*) & Key,2,(uint16_t) KeyRecordsAddress);
//	Key=0;
	if(Key == 0xC3A6){
		ret=ReadFromI2cRam((uint8_t*) & BK_3.BK3status.WriteAddress,2,(uint16_t) WriteRecordsAddress);
		ret1|=ret;

		ret=ReadFromI2cRam((uint8_t*) & BK_3.BK3status.ReadAddress,2,(uint16_t) ReadRecordsAddress);
		ret1|=ret;
	}else{
		BK_3.BK3status.WriteAddress=ADRESS_MASSIVA_DANNIX;
		ret=WriteToI2cRam((uint8_t*)( & BK_3.BK3status.WriteAddress),2,(uint16_t) WriteRecordsAddress);
		ret1|=ret;

		BK_3.BK3status.ReadAddress=ADRESS_MASSIVA_DANNIX;
		ret=WriteToI2cRam((uint8_t*)( & BK_3.BK3status.ReadAddress),2,(uint16_t) ReadRecordsAddress);
		ret1|=ret;

		Key=0xC3A6;
		ret=WriteToI2cRam((uint8_t*) & Key,2,(uint16_t) KeyRecordsAddress);
		ret1|=ret;
	}
}
//====================================================================================================================
HAL_StatusTypeDef StoreMomentToFlash(uint8_t N){
	uint32_t ADDRES;
	volatile uint8_t L;
	/* прочитаем пойнтер чтения    */
	ReadFromI2cRam((uint8_t*) & BK_3.BK3status.ReadAddress,2,(uint16_t) ReadRecordsAddress);
	/* WRP достиг верха ?    */
	if(BK_3.BK3status.WriteAddress == I2C_IMAGE_START_ADDRESS){
		/* WRP ставим в начало    */
		BK_3.BK3status.WriteAddress= RecordsStartAddress;
		/* RDP в начале  ?    */
		if(BK_3.BK3status.ReadAddress == RecordsStartAddress){
			BK_3.BK3status.ReadAddress+=sizeof(FLASH_MESSAGE_T);
		}
	}
	if( ! ((BK_3.BK3status.WriteAddress == RecordsStartAddress)
			&& (BK_3.BK3status.ReadAddress == RecordsStartAddress))){
		/* RDP один из пойнтеров не в начале      */
		/* двигаем RDP если мешает      */
		if(BK_3.BK3status.WriteAddress == BK_3.BK3status.ReadAddress){
			BK_3.BK3status.ReadAddress+=sizeof(FLASH_MESSAGE_T);
			/* RDP достиг верха ? */
			if(BK_3.BK3status.ReadAddress >= I2C_IMAGE_START_ADDRESS){
				BK_3.BK3status.ReadAddress=RecordsStartAddress;
			}
		}
	}
#if 0
	char M[50];
	L=0;
	sprintf( M, "WR P = %x    RD P = %x\r\n", BK_3.BK3status.WriteAddress,BK_3.BK3status.ReadAddress );
	USART3_SEND_MESSAGE(M);
#endif

	/* заполним рекорд     */
	FLASH_MESSAGE_T record;
	RTC_GetDateTime( & record.Dt);
	record.Numer_fazi=N;

	/* пишем рекорд     */
	HAL_StatusTypeDef RES=HAL_OK;
	RES=WriteToI2cRam((uint8_t*) & record,sizeof(FLASH_MESSAGE_T),BK_3.BK3status.WriteAddress);
	BK_3.BK3status.WriteAddress+=sizeof(FLASH_MESSAGE_T);

	/* пишем пойнтер    */
	RES|=WriteToI2cRam((uint8_t*) & BK_3.BK3status.WriteAddress,2,WriteRecordsAddress);

#if 1
	if(RES != HAL_OK){
		DebugSendUsartMessage("STORE MOM NOK\r\n");
	}else{
		DebugSendUsartMessage("STORE MOM OK\r\n");
	}
#endif
	return RES;
}
//================================================Critical============================================================
void vTask_FLASH(void * argument){
	CMD_FLASH_T A;
	BK_3L *d=argument;
	osDelay(1000);

	while(1){

		xQueueReceive(Ququ_flash, & A,portMAX_DELAY);
		xSemaphoreTake(xI2CSemaphore,portMAX_DELAY);
		switch(A){
			case store_moment_1:
				StoreMomentToFlash(1);
			break;
			case store_moment_2:
				StoreMomentToFlash(2);
			break;
			case store_moment_3:
				StoreMomentToFlash(3);
			break;
			case store_setting:
				StoreSettingsToFlash();
			break;
			case load_setting:
				LoadSettingsFromFlash();
			break;
			case update_setting:
				UpdateSettingfInFlash();
			break;
			case MSG_BOOT_OK_FLASH:
				UPDATE_Bootloader();
			break;
			case MSG_IMAGE_OK_FLASH:
				osDelay(2000);
				HAL_NVIC_SystemReset();
			break;
		}
		xSemaphoreGive(xI2CSemaphore);
		BK_3.Stek_t.FlashTaskStek=uxTaskGetStackHighWaterMark( NULL);
		vTaskDelay(30);
	}
}
//====================================================================================================================
/**
 * @brief  Процедура перезаписывает Bootloader контроллера.
 */
static void UPDATE_Bootloader(void){

	if(BK_3.firmware.size < I2C_LENGHT_IMAGE_AREA){
		FLASH_EraseInitTypeDef EraseInitStruct;
		uint64_t u64;
		uint32_t PAGEError;
		uint32_t len=BK_3.firmware.size;

		uint32_t boot_address= FLASH_BOOT_START_ADDR;
		uint32_t image_address= I2C_IMAGE_START_ADDRESS;

		/* очищаем память bootloader */
		ERASE_FlashMemory(FLASH_BOOT_START_ADDR,FLASH_BOOT_END_ADDR);

		HAL_FLASH_Unlock();
		for(int i=0 ; i < len ; i+=8){
			ReadFromI2cRam((uint8_t*) & u64,8,image_address);
			if(HAL_FLASH_Program( FLASH_TYPEPROGRAM_DOUBLEWORD,boot_address,u64) == HAL_OK){
				boot_address+=8;
				image_address+=8;
			}
		}
		HAL_FLASH_Lock();

		u64=0;
//		/* очищаем память образа программы */
		WriteToI2cRam((uint8_t*) & u64,8,I2C_IMAGE_START_ADDRESS);

	}
}
//========================================================================================================

