/*
 * settings.c
 *
 *  Created on: 10 нояб. 2017 г.
 *      Author: Poluect
 */
#include "stm32f1xx_hal.h"
#include "settings.h"
#include "INIT.h"
#include "main.h"
#include "FlashTask.h"
#include "I2C_MemDriver.h"
#include "UsartTask.h"

extern BK_3L BK_3;
static uint8_t  MASSIV[160],J;
//==================================================================================================
void FillSettingsFromUsb(uint8_t* buffer){
	USHORT N;
//	if((buffer[1] != BK_3.NumerWorkingFase) && (buffer[1] > 0) && (buffer[1] <= 3)) BK_3.NumerFaseForSwitch=1;
//	N=0;
	// todo разобраться
	memcpy( & BK_3.Setting, & buffer[1],sizeof(SETTING_T));
	N=sizeof(SETTING_T);
	memcpy( & BK_3.F1.SettingFaz, & buffer[N + 1],sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & BK_3.F2.SettingFaz, & buffer[N + 1],sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & BK_3.F3.SettingFaz, & buffer[N + 1],sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & BK_3.F_OS.SettingFaz, & buffer[N + 1],sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & BK_3.F_TOK.SettingFaz, & buffer[N + 1],sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
}
//==================================================================================================
void FillBufferFromSettings(uint8_t* buffer){
	USHORT N;
	N=0;
	memcpy( & buffer[0], & BK_3.Setting,sizeof(SETTING_T));
	N=sizeof(SETTING_T);
	memcpy( & buffer[N], & BK_3.F1.SettingFaz,sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & buffer[N], & BK_3.F2.SettingFaz,sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & buffer[N], & BK_3.F3.SettingFaz,sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & buffer[N], & BK_3.F_OS.SettingFaz,sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
	memcpy( & buffer[N], & BK_3.F_TOK.SettingFaz,sizeof(Setting_Faz_T));
	N+=sizeof(Setting_Faz_T);
}
//====================================================================================================================
bool LoadSettingsFromFlash(void){
	uint16_t S;
	bool written , ret;

	/* Прочитаем BK Settings  флаг записи сеттингов во флэш*/
	if(ReadFromI2cRam((uint8_t*) & S,2,ADRESS_MASSIVA_SETTINGOV) == HAL_OK){
		written=(S == 0x5A5B);
		/* если не записаны сеттинги -запишем*/
		if( ! written){
			/* тогда запишем начальные сеттинги */
			ret=StoreSettingsToFlash();
			return ret;
		}
	}else{
		/* ошибка I2C*/
		return false;
	}
	/* считаем массив сеттингов */
	if(ReadFromI2cRam(MASSIV,sizeof(SETTING_T)+(5*sizeof(Setting_Faz_T)),ADRESS_MASSIVA_SETTINGOV) == HAL_OK){
		ret=true;
		/* распакуем его */
		UnpackPointerToSettings(MASSIV, & J);
		DebugSendUsartMessage("Load Settings OK \r\n");
	}else{
		ret=false;
		/* ошибка I2C*/
		DebugSendUsartMessage("Load Settings FAIL \r\n");
	}
	return ret;
}
//====================================================================================================================
void PackSettingsToPointer(uint8_t *MASSIv,uint8_t * J){
	* J=0;
	memcpy( & MASSIv[( * J)], & BK_3.Setting,sizeof(SETTING_T));
	* J=( * J) + sizeof(SETTING_T);
	memcpy( & MASSIv[( * J)], & BK_3.F1.SettingFaz,sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & MASSIv[( * J)], & BK_3.F2.SettingFaz,sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & MASSIv[( * J)], & BK_3.F3.SettingFaz,sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & MASSIv[( * J)], & BK_3.F_OS.SettingFaz,sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & MASSIv[( * J)], & BK_3.F_TOK.SettingFaz,sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	MASSIv+= * J;
}
//====================================================================================================================
bool StoreSettingsToFlash(void){
	bool ret;

	/* Флаг записи во флэш */
	BK_3.Setting.FlagZapisiSettingov=0x5A5B;

	/*Записать settings в массив*/
	PackSettingsToPointer(MASSIV, & J);

	/* Записать во флэш */
	ret=(WriteToI2cRam((uint8_t*)MASSIV,J,ADRESS_MASSIVA_SETTINGOV) == HAL_OK) ? true : false;
	if( ! ret) BK_3.Setting.FlagZapisiSettingov=0;

	/*Послать сообщение*/
	if(ret){
		DebugSendUsartMessage("Store Settings OK \r\n");
	}else{
		DebugSendUsartMessage("Store Settings FAIL \r\n");
	}
	return ret;
}
//====================================================================================================================
void UnpackPointerToSettings(uint8_t *MASSIv,uint8_t * J){
	* J=0;
	memcpy( & BK_3.Setting, & MASSIv[( * J)],sizeof(SETTING_T));
	* J=( * J) + sizeof(SETTING_T);
	memcpy( & BK_3.F1.SettingFaz, & MASSIv[( * J)],sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & BK_3.F2.SettingFaz, & MASSIv[( * J)],sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & BK_3.F3.SettingFaz, & MASSIv[( * J)],sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & BK_3.F_OS.SettingFaz, & MASSIv[( * J)],sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	memcpy( & BK_3.F_TOK.SettingFaz, & MASSIv[( * J)],sizeof(Setting_Faz_T));
	* J=( * J) + sizeof(Setting_Faz_T);
	MASSIv+= * J;
}
//====================================================================================================================
bool UpdateSettingfInFlash(void){
	bool ret;

	/*Записать settings в массив*/
	PackSettingsToPointer(MASSIV, & J);

	/* Записать во флэш */
	ret=(WriteToI2cRam((uint8_t*)MASSIV,J,ADRESS_MASSIVA_SETTINGOV) == HAL_OK) ? true : false;
	if( ! ret) BK_3.Setting.FlagZapisiSettingov=0;

	/*Послать сообщение*/
	if(ret){
		DebugSendUsartMessage("Store Settings OK \r\n");
	}else{
		DebugSendUsartMessage("Store Settings FAIL \r\n");
	}
	return ret;
}
//====================================================================================================================
