/*
 * FreeModbus Library: STM32F4xx Port
 * Copyright (C) 2013 Alexey Goncharov <a.k.goncharov@ctrl-v.biz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include "stm32f1xx_hal.h"
//#include "hardware.h"
#include "hardware_init.h"
#include "port.h"
 UART_HandleTypeDef huart3;
/* ----------------------- Modbus includes ----------------------------------*/
//#include <mb.h>
#include "mbport.h"
 void USART3_portserial_IRQHandler( void );
//=================================================================================================================
void vMBPortSerialEnable( BOOL xRxEnable, BOOL xTxEnable )
{
    if( xRxEnable )
    {
    	__HAL_UART_ENABLE_IT(&huart3,UART_IT_RXNE);
    }
    else
    {
    	__HAL_UART_DISABLE_IT(&huart3,UART_IT_RXNE);
    }

    if ( xTxEnable )
    {
    	__HAL_UART_ENABLE_IT(&huart3,UART_IT_TXE);

#ifdef RTS_ENABLE
        RTS_HIGH;
#endif
    }
    else
    {
    	__HAL_UART_DISABLE_IT(&huart3,UART_IT_TXE);
    }
}
//=================================================================================================================
BOOL xMBPortSerialInit( UCHAR ucPORT, ULONG ulBaudRate,
                        UCHAR ucDataBits, eMBParity eParity )
{
	GPIO_InitTypeDef GPIO_InitStruct;

	__HAL_RCC_GPIOB_CLK_ENABLE()
	;

	USART3_RW_SET_0;

	huart3.Instance = USART3;
	huart3.Init.BaudRate =(uint32_t) ulBaudRate;
	if( ucDataBits == 9 )
	{
		huart3.Init.WordLength = UART_WORDLENGTH_9B;
	}
	else
	{
		huart3.Init.WordLength = UART_WORDLENGTH_8B;
	}

	huart3.Init.StopBits = UART_STOPBITS_1;

    switch( eParity )
    {
    case MB_PAR_NONE:
    	huart3.Init.Parity =  UART_PARITY_NONE;
        break;
    case MB_PAR_ODD:
    	huart3.Init.Parity = UART_PARITY_ODD;
        break;
    case MB_PAR_EVEN:
    	huart3.Init.Parity = UART_PARITY_EVEN;
        break;
    default:
    	huart3.Init.Parity = UART_PARITY_NONE;
        break;
    };
	huart3.Init.Parity = UART_PARITY_NONE;
	huart3.Init.Mode = UART_MODE_TX_RX;
	huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart3.Init.OverSampling = UART_OVERSAMPLING_16;
	HAL_UART_Init( & huart3 );

    HAL_NVIC_SetPriority(USART3_IRQn, 7, 0);
    HAL_NVIC_EnableIRQ(USART3_IRQn);

    vMBPortSerialEnable( TRUE, TRUE );

#ifdef RTS_ENABLE
    RTS_INIT;
#endif
    return TRUE;
}
//=================================================================================================================
BOOL xMBPortSerialPutByte( CHAR ucByte )
{
	CHAR A;
	A = ucByte;
	USART3_RW_SET_1;

//    USART_SendData( USART3, (uint16_t) ucByte );
//	  HAL_UART_Transmit(& huart3, &A, 1,10);

	USART3->DR = ( uint16_t ) ucByte;

	return TRUE;
}
//=================================================================================================================
BOOL xMBPortSerialGetByte ( CHAR * pucByte )
{

//    *pucByte = (CHAR) USART_ReceiveData( USART3 );

	* pucByte = ( CHAR ) ( USART3->DR & ( uint8_t ) 0x00FF );

//	  HAL_UART_Receive(& huart3, &A, 1,10);

	return TRUE;
}
//=================================================================================================================
void USART3_portserial_IRQHandler( void )
{

//ПРОЦЕДУРА IRQ USART3 ДЛЯ MODBUSA - НЕ ИПОЛЬЗУЕТ СТАНДАРТНЫУ ПРОЦЕДУРЫ HAL

	uint32_t tmp_flag = 0 , tmp_it_source = 0;
// -------------------ПРИЕМ БФЙТА ------------------------------------
	tmp_flag = __HAL_UART_GET_FLAG( & huart3 , UART_FLAG_RXNE );
	tmp_it_source = __HAL_UART_GET_IT_SOURCE( & huart3 , UART_IT_RXNE );

	if ( ( tmp_flag != RESET ) && ( tmp_it_source != RESET ))
	{
		__HAL_UART_CLEAR_FLAG( & huart3 , UART_FLAG_RXNE );
		pxMBFrameCBByteReceived();
	}
// -------------------ПУСТОТА ПЕРЕАТЧИКА ------------------------------------
	tmp_flag = __HAL_UART_GET_FLAG( & huart3 , UART_FLAG_TXE );
	tmp_it_source = __HAL_UART_GET_IT_SOURCE( & huart3 , UART_IT_TXE );

	if ( ( tmp_flag != RESET ) && ( tmp_it_source != RESET ))
	{
		__HAL_UART_CLEAR_FLAG( & huart3 , UART_FLAG_TXE );
		pxMBFrameCBTransmitterEmpty();
	}

}
//=================================================================================================================
