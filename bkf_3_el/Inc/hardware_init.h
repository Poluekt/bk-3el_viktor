/*
 * stm32_103dc_init.h
 *
 *  Created on: 25 окт. 2013 г.
 *      Author: develop2
 */
#include "stm32f1xx_hal.h"

#include "stdbool.h"
#include <stdio.h>
#include <string.h>
#include "cmsis_os.h"

#ifndef STM32_103DC_INIT_H_
#define STM32_103DC_INIT_H_
#define MASS_MEMORY_START     0x04002000
#define BULK_MAX_PACKET_SIZE  0x00000040
#define LED_ON                0xF0
#define LED_OFF               0xFF

#define USART3_TX_PIN                		GPIO_PIN_10
#define USART3_TX_GPIO_PORT          		GPIOB

#define USART3_RX_PIN                		GPIO_PIN_11
#define USART3_RX_GPIO_PORT          		GPIOB

#define USART3_RW_PIN                		GPIO_PIN_14
#define USART3_RW_GPIO_PORT          		GPIOB

#define USART3_RW_SET_0                     HAL_GPIO_WritePin (GPIOB, GPIO_PIN_14 ,GPIO_PIN_RESET)
#define USART3_RW_SET_1                     HAL_GPIO_WritePin (GPIOB, GPIO_PIN_14 ,GPIO_PIN_SET)

//------------------------------------------------------------------------------------
#define ADC_F1_Pin      GPIO_PIN_2
#define ADC_F2_Pin      GPIO_PIN_1
#define ADC_F3_Pin      GPIO_PIN_3
#define ADC_OS_Pin      GPIO_PIN_0
#define ADC_TOK_Pin     GPIO_PIN_1
//------------------------------------------------------------------------------------
#define SDA_PIN                		GPIO_PIN_6
#define SDA_GPIO_PORT          		GPIOB
#define SDA_GPIO_CLK           		RCC_APB2Periph_GPIOB

#define SCL_PIN                		GPIO_PIN_7
#define SCL_GPIO_PORT          		GPIOB
//------------------------------------------------------------------------------------

#define RELAY_1_PIN                		GPIO_PIN_4
#define RELAY_2_PIN                		GPIO_PIN_6
#define RELAY_3_PIN                		GPIO_PIN_7
#define RELAY_GPIO_PORT          		GPIOA

#define RELAY_1_ON                		HAL_GPIO_WritePin (GPIOA, GPIO_PIN_4 ,GPIO_PIN_SET)
#define RELAY_1_OFF                		HAL_GPIO_WritePin (GPIOA, GPIO_PIN_4 ,GPIO_PIN_RESET)
#define RELAY_2_ON                		HAL_GPIO_WritePin (GPIOA, GPIO_PIN_6 ,GPIO_PIN_SET)
#define RELAY_2_OFF                		HAL_GPIO_WritePin (GPIOA, GPIO_PIN_6 ,GPIO_PIN_RESET)
#define RELAY_3_ON                		HAL_GPIO_WritePin (GPIOA, GPIO_PIN_7 ,GPIO_PIN_SET)
#define RELAY_3_OFF                		HAL_GPIO_WritePin (GPIOA, GPIO_PIN_7 ,GPIO_PIN_RESET)

//------------------------------------------------------------------------------------
#define WC_H         GPIOB->BSRR = GPIO_PIN_9
#define WC_L         GPIOB->BRR  = GPIO_PIN_9
//------------------------------------------------------------------------------------
#define LED1_PIN                		GPIO_PIN_13
#define LED2_PIN                		GPIO_PIN_2
#define LED2_GPIO_PORT          		GPIOB
#define LED1_GPIO_PORT                  GPIOC

#define  LED1_ON                		HAL_GPIO_WritePin (LED1_GPIO_PORT, LED1_PIN ,GPIO_PIN_RESET)
#define  LED1_OFF                		HAL_GPIO_WritePin (LED1_GPIO_PORT, LED1_PIN ,GPIO_PIN_SET)
#define  LED1_TOGGLE                	HAL_GPIO_TogglePin (LED1_GPIO_PORT, LED1_PIN )

#define LED2_ON                  		HAL_GPIO_WritePin (LED2_GPIO_PORT, LED2_PIN ,GPIO_PIN_RESET)
#define LED2_OFF                		HAL_GPIO_WritePin (LED2_GPIO_PORT, LED2_PIN ,GPIO_PIN_SET)
#define LED2_TOGGLE               		HAL_GPIO_TogglePin (LED2_GPIO_PORT, LED2_PIN )
//------------------------------------------------------------------------------------
#define LIGHT_PIN                		GPIO_PIN_12
#define LIGHT_GPIO_PORT          		GPIOB
#define LIGHT_ON                  		HAL_GPIO_WritePin (LIGHT_GPIO_PORT, LIGHT_PIN ,GPIO_PIN_SET)
#define LIGHT_OFF                		HAL_GPIO_WritePin (LIGHT_GPIO_PORT, LIGHT_PIN ,GPIO_PIN_RESET)
#define LIGHT_TOGGLE               		HAL_GPIO_TogglePin (LIGHT_GPIO_PORT, LIGHT_PIN )

#define LOW_LEVEL_ANALOG_WATCHDOG 100
#define HIGH_LEVEL_ANALOG_WATCHDOG  3950

void ADC_Init ( void );

#endif /* STM32_103DC_INIT_H_ */
