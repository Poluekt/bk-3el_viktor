/*
 * usb_handler.c
 *
 *  Created on: 17 нояб. 2017 г.
 *      Author: Poluect
 */

/*
 * usb_handler.c
 *
 *  Created on: 30 сент. 2017 г.
 *      Author: develop2
 */

//#include <memory_task.h>
#include "usb_device.h"
#include "usbd_cdc.h"
#include "INIT.h"
#include "main.h"
#include "settings.h"
#include "rtc.h"
#include "FlashTask.h"
#include "handler.h"
#include <I2C_MemDriver.h>
/*-------------------------------------------------------------------------------------------------*/
#define START_BYTE		(0xAB)

/*----------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------------*/
extern USBD_HandleTypeDef hUsbDeviceFS;
extern uint8_t UserRxBufferFS[];
extern BK_3L BK_3;
/*----------------------------------------------------------------------*/
static uint32_t write_address;
uint32_t crc32;
X_PACKET_T packet;
/*----------------------------------------------------------------------*/
static void UsbSendData(void);
static uint32_t STM32_SOFT_CRC32_1(uint8_t* sours,uint32_t Len);
uint16_t GetPoolData(uint8_t * buffer);
/*======================================================================*/
void CopyImageBlock(uint8_t * buffer){
	/*  захватим семафор   */
	xSemaphoreTake(xI2CSemaphore,portMAX_DELAY);

	WriteToI2cRam(buffer,32,write_address);
	write_address+=32;
	/*  отдадим семафор   */
	xSemaphoreGive(xI2CSemaphore);
}
/*----------------------------------------------------------------------*/
uint8_t GetCrc8(char *psource,uint8_t size){
	unsigned char res=0;
	unsigned char data;
	unsigned char i;
	unsigned char temp;
	while(size--){
		data= * psource++;
		for(i=0; i < 8 ; i++){
			temp=res << 7;
			res>>=1;
			if((temp >> 7) ^ (data & 0x01)){
				res^=0x8c;
				res|=0x80;
			}
			data>>=1;
		}
	}
	return res;
}
/*------------------------------------------------------------------*/
 uint16_t GetPoolData(uint8_t * buffer){
	uint32_t LenWrittenData;
	uint16_t len,rest,suppl;
	BaseType_t xHigherPriorityTaskWoken= pdFALSE;
	/*  захватим семафор   */
	xSemaphoreTake(xI2CSemaphore,portMAX_DELAY);
	/*  прочитаем пойнтер чтения   */
	ReadFromI2cRam((uint8_t*) & BK_3.BK3status.ReadAddress,2,(uint16_t) ReadRecordsAddress);
	/* определим объем записанных данных   */
	LenWrittenData=
			(BK_3.BK3status.WriteAddress >= BK_3.BK3status.ReadAddress) ?
					(BK_3.BK3status.WriteAddress - BK_3.BK3status.ReadAddress) :
					((I2C_IMAGE_START_ADDRESS - ADRESS_MASSIVA_DANNIX)
							- (BK_3.BK3status.ReadAddress - BK_3.BK3status.WriteAddress));
	/* если че - возврат   */
	if(LenWrittenData){
		/*  отдадим семафор   */
		xSemaphoreGiveFromISR(xI2CSemaphore,& xHigherPriorityTaskWoken);
		return 0;
	}
	/* определим длину передаваемых данных   */
	len=(LenWrittenData >= (8 * sizeof(FLASH_MESSAGE_T))) ? (8 * sizeof(FLASH_MESSAGE_T)) : LenWrittenData;
	len=(len / sizeof(FLASH_MESSAGE_T)) * sizeof(FLASH_MESSAGE_T);
	/* закольцовываемся ежели че   */
	rest=suppl=0;
	if((BK_3.BK3status.ReadAddress + len) >= I2C_IMAGE_START_ADDRESS){
		rest=I2C_IMAGE_START_ADDRESS - BK_3.BK3status.ReadAddress;
		suppl=len - rest;
	}
	/* читаем пул данных   */
	if(rest){
		ReadFromI2cRam(buffer,(uint32_t)rest,(uint32_t)BK_3.BK3status.ReadAddress);
		buffer+=rest;
		BK_3.BK3status.ReadAddress+=rest;
	}
	if(suppl){
		ReadFromI2cRam(buffer,(uint32_t)suppl,(uint32_t) ADRESS_MASSIVA_DANNIX);
		buffer+=suppl;
		BK_3.BK3status.ReadAddress+=suppl;
	}
	/*  отдадим семафор   */
	xSemaphoreGiveFromISR(xI2CSemaphore,& xHigherPriorityTaskWoken);
	return len;
}
/*------------------------------------------------------------------*/
void ConfirmData(uint8_t* buf){
	BaseType_t xHigherPriorityTaskWoken= pdFALSE;
	/*  захватим семафор   */
	xSemaphoreTake(xI2CSemaphore,portMAX_DELAY);
	/*  прочитаем пойнтер чтения   */
	ReadFromI2cRam((uint8_t*) & BK_3.BK3status.ReadAddress,2,(uint16_t) ReadRecordsAddress);
	/* принятое кол-во структур и байт   */
	uint16_t len=( * buf) * sizeof(FLASH_MESSAGE_T);
	uint16_t suppl=
			((BK_3.BK3status.ReadAddress + len) > I2C_IMAGE_START_ADDRESS) ?
					sizeof(FLASH_MESSAGE_T) - (I2C_IMAGE_START_ADDRESS - BK_3.BK3status.ReadAddress) : 0;
	BK_3.BK3status.ReadAddress=(suppl) ? (ADRESS_MASSIVA_DANNIX + suppl) : (BK_3.BK3status.ReadAddress + len);
	/*  сохраним пойнтер чтения   */
	WriteToI2cRam((uint8_t*) & BK_3.BK3status.ReadAddress,2,(uint16_t) ReadRecordsAddress);
	/*  отдадим семафор   */
	xSemaphoreGiveFromISR(xI2CSemaphore,& xHigherPriorityTaskWoken);
}
/*------------------------------------------------------------------*/
void ParseInformatProtocol(uint8_t* buf,uint32_t length){
	DATATIME_T DT;
	uint32_t crc32;
	crc32=0xffffffff;
	static uint32_t PAGEError=0;

	memcpy( & packet, & UserRxBufferFS,length);
	uint8_t crc8=GetCrc8((char*) & UserRxBufferFS,packet.length + 3);
	if(packet.id != 0xAB || packet.buffer[packet.length] != crc8){
		UsbSendData();
		return;
	}
	switch(packet.command){
		case READ_ID:
			packet.length=strlen(BK_3.id) + 1;
			strcpy((char *) & packet.buffer,(char *) & BK_3.id);
		break;
		case GET_SETTING:
			PackSettingsToPointer(packet.buffer, & packet.length);
		break;
		case GET_STATE:
			StoreStatusToPointer((UCHAR *) & packet.buffer, & packet.length);
		break;
		case GET_POOL_DATA:
			packet.length=GetPoolData(& packet.buffer[0]);
		break;
		case CONFIRM_DATA:
            ConfirmData(&packet.buffer[0]);
		break;
		case SET_TIME:
			DT.sDate.Year=packet.buffer[0];
			DT.sDate.Month=packet.buffer[1];
			DT.sDate.Date=packet.buffer[2];
			DT.sTime.Hours=packet.buffer[3];
			DT.sTime.Minutes=packet.buffer[4];
			DT.sTime.Seconds=packet.buffer[5];
			packet.buffer[0]=1;
			RTC_SetDateTime( & DT);
		break;
		case SET_SETTTINGS: {
			uint8_t J=0;
			UnpackPointerToSettings((uint8_t *) & packet.buffer, & J);
			/* записать уставки */
			SendMessageToFlash(update_setting);
		}
		break;
		case SET_SWITCH_FAZE: {
			uint8_t S=packet.buffer[0];
			if((S != BK_3.NumerWorkingFase) && (S >= 1) && (S <= 3)){
				BK_3.BK3status.NumerFaseForSwitch=S;
				break;
			}
		}
		case START_DOWNLOADER:
			/* получим длину и crc32 образа */
			StartDownload( & packet.buffer[0]);
		break;
		case WRITE_IMAGE_BLOCK:
			/*  Копирируем образ в область образа программы .Пишем по 32 байта*/
			CopyImageBlock( & packet.buffer[0]);
		break;
		case STOP_DOWNLOADER:
			packet.buffer[0]=1;
		break;
		case SOFTWARE_RESET:
		case SAVE_BOOTLOADER:
			UploadSoft(packet.command);
			packet.buffer[0]=1;
		break;
	}
	UsbSendData();
}
/*------------------------------------------------------------------*/
void StartDownload(uint8_t * buffer){
	memcpy( & BK_3.firmware.size,buffer,4);
	memcpy( & BK_3.firmware.crc32, & buffer[4],4);
	write_address=I2C_IMAGE_START_ADDRESS;
}
/*------------------------------------------------------------------*/
static uint32_t STM32_SOFT_CRC32_1(uint8_t* sours,uint32_t Len){
//	uint32_t crc32=0xffffffff;

	for(uint32_t j=0 ; j < Len ; j+=4){
		crc32=crc32 ^ * ((uint32_t*) & sours[j]);

		for(unsigned char i=0 ; i < 32 ; i++)
			if(crc32 & 0x80000000) crc32=(crc32 << 1) ^ 0x04C11DB7;
			else crc32=(crc32 << 1);
	}
	return (crc32);
}
/*------------------------------------------------------------------*/
void UploadSoft(Commands command){
	uint64_t u64;
	uint32_t lenght=BK_3.firmware.size , len;
	uint32_t image_address= I2C_IMAGE_START_ADDRESS;

	xSemaphoreTake(xI2CSemaphore,portMAX_DELAY);
	while(lenght){
		len=(lenght > 8) ? 8 : lenght;
		ReadFromI2cRam((uint8_t*) & u64,len,image_address);
		image_address+=len;
		crc32=STM32_SOFT_CRC32_1((uint8_t*) & u64,len);
	}
	/* если образ правильный перезагрузка */
	if(crc32 == BK_3.firmware.crc32){
		/* записать образ */
		if(command == SAVE_BOOTLOADER){
			SendMessageToFlash(MSG_BOOT_OK_FLASH);
		}else{
			SendMessageToFlash(MSG_IMAGE_OK_FLASH);
		}
	}else{
		/* очищаем память образа программы */
		u64=0;
		WriteToI2cRam((uint8_t*) & u64,8,I2C_IMAGE_START_ADDRESS);
	}
	xSemaphoreGive(xI2CSemaphore);
}
/*---------------------------  ---------------------------------------*/

void UsbHandler(uint8_t* buf,uint32_t *length){
	ParseInformatProtocol(buf, * length);
}
/*------------------------------------------------------------------*/
static void UsbSendData(void){
	packet.buffer[packet.length]=GetCrc8((uint8_t*) & packet,packet.length + 3);
	USBD_CDC_SetTxBuffer( & hUsbDeviceFS,(uint8_t*) & packet,packet.length + 4);

	USBD_CDC_TransmitPacket( & hUsbDeviceFS);
	USBD_CDC_SetRxBuffer( & hUsbDeviceFS, & UserRxBufferFS[0]);
	USBD_CDC_ReceivePacket( & hUsbDeviceFS);
}
/************************ (C) copyright sapfir.biz *************************/
