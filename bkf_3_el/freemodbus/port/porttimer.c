/*
 * FreeModbus Library: STM32F4xx Port
 * Copyright (C) 2013 Alexey Goncharov <a.k.goncharov@ctrl-v.biz>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */


/* ----------------------- Platform includes --------------------------------*/
#include "port.h"
#include "stm32f1xx_hal.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mbport.h"
TIM_HandleTypeDef htim3;
extern ADC_HandleTypeDef hadc2;
//=================================================================================================================
BOOL xMBPortTimersInit( USHORT usTim1Timerout50us )
{

	  TIM_ClockConfigTypeDef sClockSourceConfig;
	  TIM_MasterConfigTypeDef sMasterConfig;

	  htim3.Instance = TIM3;
	  htim3.Init.Prescaler = (72000000/20000)-1;
	  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
	  htim3.Init.Period = ( (uint32_t) usTim1Timerout50us ) - 1;
	  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  HAL_TIM_Base_Init(&htim3);

	  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
	  HAL_TIM_ConfigClockSource(&htim3, &sClockSourceConfig);

	  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	  HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig);

	  HAL_TIM_Base_Start_IT(&htim3);

    return TRUE;
}
//=================================================================================================================
void vMBPortTimersEnable()
{
	__HAL_TIM_SET_COUNTER(&htim3,0);
	__HAL_TIM_ENABLE(&htim3);
}
//=================================================================================================================
void vMBPortTimersDisable()
{
	__HAL_TIM_DISABLE(&htim3);
}

//=================================================================================================================
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
//-----------------------------------------------------------
	if (htim->Instance == TIM3)
	{
		( void ) pxMBPortCBTimerExpired();
	}
//-----------------------------------------------------------
	if (htim->Instance== TIM4)
	{
		 HAL_ADCEx_InjectedStart_IT(&hadc2);
	}
//-----------------------------------------------------------
}
//=================================================================================================================
