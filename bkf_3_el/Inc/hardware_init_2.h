/*
 * hardware_init_2.h
 *
 *  Created on: 30 июня 2016 г.
 *      Author: Viktor
 */

#ifndef INC_HARDWARE_INIT_2_H_
#define INC_HARDWARE_INIT_2_H_

#include "usbd_def.h"
#include "main_proc.h"
#include "handler.h"
#include "rtc.h"



void device_init ( void );
void USART3_Init ( void );
void USART3_SEND_MESSAGE ( char * MESSAGE );
void RelayInit ( void );
void StructuresInit(void);

#endif /* INC_HARDWARE_INIT_2_H_ */
