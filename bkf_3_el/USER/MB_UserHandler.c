#include "BK_3el.h"

#define REG_HOLDING_START   40000
#define REG_HOLDING_NREGS   130

#define REG_INPUT_START   30000
#define REG_INPUT_NREGS   70

extern BK_3L BK_3;
extern const uint8_t * AddrModbInputRg[44];
extern const uint8_t *AddrModbHoldRg[40];

static void FillHoldingRg(UCHAR *pucRegBuffer,int iRegIndex);
static void GetFromHoldRg(UCHAR * pucRegBuffer,int iRegIndex);
void FillInputRg(UCHAR * pucRegBuffer,uint32_t index);
void StoreStatusToPointer(UCHAR * pucRegBuffer,uint8_t * J);

//=====================================================================================================================
eMBErrorCode eMBRegInputCB(UCHAR * pucRegBuffer,USHORT usAddress,USHORT usNRegs){
	uint16_t US;
	eMBErrorCode eStatus=MB_ENOERR;
	int iRegIndex;

	if((usAddress > REG_INPUT_START) && (usAddress + usNRegs <= REG_INPUT_START + REG_INPUT_NREGS)){

		iRegIndex=(int)(usAddress - REG_INPUT_START);
		while(usNRegs > 0){
//  usAddress начинается с 30000 - это iRegIndex=1
			if((iRegIndex >= 1) && (iRegIndex < 39)){
				FillInputRg(pucRegBuffer,iRegIndex);
			}else if(iRegIndex == 40){
				uint8_t J=0;
				StoreStatusToPointer(pucRegBuffer, & J);
			}else{

			}
			iRegIndex++;
			usNRegs--;
			pucRegBuffer+=2;
		}
		return MB_ENOERR;
	}
	return MB_ENOREG;
}

eMBErrorCode eMBRegHoldingCB(UCHAR * pucRegBuffer,USHORT usAddress,USHORT usNRegs,eMBRegisterMode eMode){
	uint16_t US;
	eMBErrorCode eStatus=MB_ENOERR;
	volatile int iRegIndex;

	if((usAddress > REG_HOLDING_START) && (usAddress + usNRegs <= REG_HOLDING_START + REG_HOLDING_NREGS)){

		iRegIndex=(int)(usAddress - REG_HOLDING_START);

//		iRegIndex=usAddress;

		switch(eMode){
			case MB_REG_READ:
//-------------------------------------------------------------------------------------------------------
				while(usNRegs > 0){
//  usAddress начинается с 40000 - это iRegIndex=1
					if((iRegIndex >= 1) && (iRegIndex < 43)){
						FillHoldingRg(pucRegBuffer,iRegIndex);
					}else if(iRegIndex == 50){
						uint8_t J=0;
						PackSettingsToPointer(pucRegBuffer, & J);
					}else{
						* pucRegBuffer=0;
						* (pucRegBuffer + 1)=0;
					}
					iRegIndex+=1;
					usNRegs--;
					pucRegBuffer+=2;
				}
			break;
			case MB_REG_WRITE:
				while(usNRegs > 0){
					if((iRegIndex >= 1) && (iRegIndex < 43)){
						GetFromHoldRg(pucRegBuffer,iRegIndex);
					}else if(iRegIndex == 50){
						uint8_t J=0;
						UnpackPointerToSettings(pucRegBuffer, & J);
						SendMessageToFlash(update_setting);
					}else if(iRegIndex == 120){
						StartDownload(pucRegBuffer);
					}else if(iRegIndex == 130){
						CopyImageBlock(pucRegBuffer);
					}else if(iRegIndex == 150){
						UploadSoft(SOFTWARE_RESET);
					}else if(iRegIndex == 152){
						UploadSoft(SAVE_BOOTLOADER);
					}else if(iRegIndex == 154){
						uint8_t S= * (pucRegBuffer + 1);
						if((S != BK_3.NumerWorkingFase) && (S >= 1) && (S <= 3)){
							BK_3.BK3status.NumerFaseForSwitch=S;
						}

					}else{
					}
					iRegIndex++;
					usNRegs--;
					pucRegBuffer+=2;
				}
			break;
		}
//----------------------------------------------------------------configUSE_TRACE_FACILITY --------------------------------------
	}else{
		eStatus=MB_ENOREG;
	}
	return eStatus;
}
//====================================================================================================================
eMBErrorCode eMBRegCoilsCB(UCHAR * pucRegBuffer,USHORT usAddress,USHORT usNCoils,eMBRegisterMode eMode){
	return MB_ENOREG;
}
//=====================================================================================================================
eMBErrorCode eMBRegDiscreteCB(UCHAR * pucRegBuffer,USHORT usAddress,USHORT usNDiscrete){
	return MB_ENOREG;
}
//======================================================union==============================================================
void FillInputRg(UCHAR * pucRegBuffer,uint32_t iRegIndex){
	uint16_t US;
	if((iRegIndex >= 1) && (iRegIndex <= 11)) US=(uint16_t)( * ((uint8_t*)AddrModbInputRg[iRegIndex]));
	if((iRegIndex >= 12) && (iRegIndex <= 39)) US= * ((uint16_t*)AddrModbInputRg[iRegIndex]);
	* pucRegBuffer=(uint8_t)(US >> 8);
	* (pucRegBuffer + 1)=(uint8_t)US;
}
//========================================================================================================
static void FillHoldingRg(UCHAR *pucRegBuffer,int iRegIndex){
	uint16_t US;
	if((iRegIndex >= 1) && (iRegIndex <= 3)) US=(uint16_t)( * ((uint8_t*)AddrModbHoldRg[iRegIndex]));
	if((iRegIndex >= 4) && (iRegIndex <= 43)) US= * ((uint16_t*)AddrModbHoldRg[iRegIndex]);
	* pucRegBuffer=(uint8_t)(US >> 8);
	* (pucRegBuffer + 1)=(uint8_t)US;
}
//=====================================================================================================================
static void GetFromHoldRg(UCHAR *pucRegBuffer,int iRegIndex){
	if((iRegIndex >= 1) && (iRegIndex <= 3)){
		uint8_t S= * (pucRegBuffer + 1);
		* (uint8_t*)AddrModbHoldRg[iRegIndex]=S;
		* (uint8_t*)(AddrModbHoldRg[iRegIndex] + 1)=0;
	}else{
		if((iRegIndex >= 4) && (iRegIndex <= 43)){
			* (uint8_t*)AddrModbHoldRg[iRegIndex]=( * pucRegBuffer) << 8;
			* (uint8_t*)(AddrModbHoldRg[iRegIndex] + 1)=( * (pucRegBuffer + 1)) << 8;
		}
	}
}
//===============================================================================================================
void StoreStatusToPointer(UCHAR * pucRegBuffer,uint8_t * J){
	memcpy(pucRegBuffer, & BK_3.BK3status,sizeof(BK_3_T));
	* J+=sizeof(BK_3_T);
	pucRegBuffer+=sizeof(BK_3_T);
	memcpy(pucRegBuffer, & BK_3.F1.StatusFaz,sizeof(Status_Faz_T));
	* J+=sizeof(Status_Faz_T);
	pucRegBuffer+=sizeof(Status_Faz_T);
	memcpy(pucRegBuffer, & BK_3.F2.StatusFaz,sizeof(Status_Faz_T));
	* J+=sizeof(Status_Faz_T);
	pucRegBuffer+=sizeof(Status_Faz_T);
	memcpy(pucRegBuffer, & BK_3.F3.StatusFaz,sizeof(Status_Faz_T));
	* J+=sizeof(Status_Faz_T);
	pucRegBuffer+=sizeof(Status_Faz_T);
	memcpy(pucRegBuffer, & BK_3.F_OS.StatusFaz,sizeof(Status_Faz_T));
	* J+=sizeof(Status_Faz_T);
	pucRegBuffer+=sizeof(Status_Faz_T);
	memcpy(pucRegBuffer, & BK_3.F_TOK.StatusFaz,sizeof(Status_Faz_T));
	* J+=sizeof(Status_Faz_T);
	pucRegBuffer+=sizeof(Status_Faz_T);
}
//===============================================================================================================
