/*
 * settings.h
 *
 *  Created on: 10 нояб. 2017 г.
 *      Author: Poluect
 */

#ifndef CLASSES_INC_SETTINGS_H_
#define CLASSES_INC_SETTINGS_H_

#include "stdbool.h"

bool LoadSettingsFromFlash(void);
void PackSettingsToPointer(uint8_t *MASSIv,uint8_t * J);
bool StoreSettingsToFlash(void);
void UnpackPointerToSettings(uint8_t *MASSIv,uint8_t * J);
bool UpdateSettingfInFlash(void);

#endif /* CLASSES_INC_SETTINGS_H_ */
