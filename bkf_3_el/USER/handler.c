/*
 * handler.c
 *
 *  Created on: 25 Р С•Р С”РЎвЂљ. 2013 Р С–.
 *      Author: develop2
 */

#include "BK_3el.h"

extern BK_3L BK_3;
extern unsigned char FLAG_CMD_SWITCH;
extern xQueueHandle Ququ_flash;
extern xQueueHandle Ququ_indik;

//==================================================================================================
uint8_t get_crc8(uint8_t *psource,uint16_t size){
	unsigned char res=0;
	unsigned char data;
	unsigned char i;
	unsigned char temp;
	while(size--){
		data= * psource++;
		for(i=0; i < 8 ; i++){
			temp=res << 7;
			res>>=1;
			if((temp >> 7) ^ (data & 0x01)){
				res^=0x8c;
				res|=0x80;
			}
			data>>=1;
		}
	}
	return res;
}
//===========================================================================================================

void SendMessageToIndikacia(CMD_INDIKACII_T A){
	CMD_INDIKACII_T B;
	B=A;
	xQueueSendToBack(Ququ_indik, & B,0);
}
//============================================================================================================
void SendMessageToFlash(CMD_FLASH_T A){
	uint16_t B;
	B=A;
//	xQueueSendToBack(Ququ_flash, & B,0);
	xQueueSendToBack(Ququ_flash, & B,portMAX_DELAY);
}
//============================================================================================================

