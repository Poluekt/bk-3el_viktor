/*
 * 24C32.h
 *
 *  Created on: 10 нояб. 2017 г.
 *      Author: Poluect
 */

#ifndef CLASSES_INC_24C32_H_
#define CLASSES_INC_24C32_H_

#include "stdbool.h"
#include "stm32f1xx_hal.h"

//#define Mem24C32
#define Mem24C512
//#define Mem24CM01


#ifdef Mem24C32
#define I2C_DEVICEADDRESS     0xA0
#define I2C_PageSize  32
#define VALUE_I2CMEM   0x1000
#else
#ifdef Mem24C512
#define I2C_DEVICEADDRESS     0xA0
#define I2C_PageSize  128
#define VALUE_I2CMEM   64*1024
#else
#ifdef Mem24CM01
#define I2C_DEVICEADDRESS     0xA0
#define I2C_PageSize  256
#define VALUE_I2CMEM   128*1024
#endif
#endif
#endif

void I2C_Init(void);
HAL_StatusTypeDef WriteToI2cRam(uint8_t* pBuffer,uint32_t length,uint32_t WriteAddress);
HAL_StatusTypeDef ReadFromI2cRam(uint8_t* pBuffer, uint32_t length, uint32_t WriteAddress);

#endif /* CLASSES_INC_24C32_H_ */
