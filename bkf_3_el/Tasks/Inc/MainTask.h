/*
 * freertos.h
 *
 *  Created on: 3 июля 2016 г.
 *      Author: Viktor
 */

#ifndef INC_MAINTASK_H_
#define INC_MAINTASK_H_

#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
#include "INIT.h"
#include "LCD.h"

void DebugSendUsartMessage(char *  str);
void CALC_IND_VALUES ( void );
void vTask_EMULATION ( void const * argument );
void IndikaciaTokNapr(void);;
#endif /* INC_MAINTASK_H_ */
