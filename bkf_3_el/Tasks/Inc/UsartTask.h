/*
 * UsartTask.h
 *
 *  Created on: 29 окт. 2017 г.
 *      Author: Poluect
 */

#ifndef TASKS_INC_USARTTASK_H_
#define TASKS_INC_USARTTASK_H_

void vTask_UART(void const * argument);
void DebugSendUsartMessage(char *  str);
void Usart3SendMessageOnline ( char * MESSAGE );
#endif /* TASKS_INC_USARTTASK_H_ */
