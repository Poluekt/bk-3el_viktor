/*
 * LCD.h
 *
 *  Created on: 22 янв. 2016 г.
 *      Author: Viktor
 */

#ifndef CLASSES_INC_LCD_H__
#define CLASSES_INC_LCD_H__

#include "stm32f1xx_hal.h"
#include "mxconstants.h"
//============================================================================================

#define E_ON       GPIOA->BSRR = E_Pin

#define E_OFF      GPIOA->BSRR = (uint32_t)E_Pin << 16

#define RS_ON      GPIOB->BSRR = RS_Pin
#define RS_OFF     GPIOB->BSRR = (uint32_t)RS_Pin << 16

#define RW_ON      GPIOA->BSRR = RW_Pin
#define RW_OFF     GPIOA->BSRR = (uint32_t)RW_Pin << 16

//============================================================================================


//#define SCL_ON       GPIOB->BSRR = DB5_Pin
//#define SCL_OFF      GPIOB->BSRR = (uint32_t)DB5_Pin << 16
//
//#define SDO_ON       GPIOB->BSRR = DB6_Pin
//#define SDO_OFF      GPIOB->BSRR = (uint32_t)DB6_Pin << 16

//#define CS_ON       GPIOB->BSRR = DB4_Pin
//#define CS_OFF      GPIOB->BSRR = (uint32_t)DB4_Pin << 16

//#define SDI_VALUE       GPIOB->IDR & DB7_Pin

//#define DB0_Pin GPIO_PIN_0
//#define DB1_Pin GPIO_PIN_1
//#define DB2_Pin GPIO_PIN_2
//#define DB3_Pin GPIO_PIN_3


 void LcdDelay ( uint32_t volatile  DELAY );
void LcdInit ( void );
void WriteStrToLcd ( uint8_t LINE_NUMBER , uint8_t POS , char * str );
void LcdWriteCmd ( unsigned char CMD );
void LcdWriteData ( unsigned char DAT );
void WriteLcdHighNibble ( uint8_t NIBBLE );
#endif /* CLASSES_INC_LCD_H__ */

